#!/usr/bin/env python

''' OPTIMIZATION TOWARDS SADLLE POINT FROM AN OPTIMIZED FIT'''

import sys
import yaml
sys.path.append('source/')
from constants import *
from basis_function import *
from potential import *

import numpy as np
from scipy.optimize import minimize

if len(sys.argv) < 2:
    raise Exception("Syntax: find_minimum.py expansion_file")

filename_expansion = sys.argv[1]

initial_guess=[ 2.23538837, 2.62609836 ,1.84677939,-0.39070911,-0.21888239, 0.05351355]
initial_guess=[2.21,2.68,1.82,-0.5,-0.25,3.07] 
bnds=((None,None),(None,None),(None,None),(None,None),(None,None),(1.42,3.14))
def output_expansion(all_coordinates):
  expansion = yaml.load(open(filename_expansion,'r'), Loader=yaml.Loader)

  output_expansion=0.0
  #For each term:
  for current_term in expansion['full_list_of_terms']:
    if expansion[str(current_term)]['status']=="Optimized": #Do not consider terms that were skipped
      current_order=len(current_term)
      current_functional_sequence=expansion[str(current_term)]['functional_sequence'] #what is the sequence of functionals for that term
      current_alpha_basis=np.zeros(current_order)
      t=0
      for dimension in current_term:
        current_alpha_basis[t]=expansion['alpha_basis'][dimension] 
        t+=1
#      current_alpha_basis=expansion[str(current_term)]['alpha_basis']
      current_minimas=expansion[str(current_term)]['minimas']
      current_coordinates_types=expansion[str(current_term)]['coordinates_types']
      current_expansion_coeff=expansion[str(current_term)]['expansion_coeff']
      #What should I supply to the function potential given the term?

      if current_order>1:
        point=np.zeros(current_order)
        for i in range(current_order):
          point[i]=all_coordinates[current_term[i]]
      else:
        point=all_coordinates[current_term]
      current_y_output,dump1=potential(point,current_order,current_coordinates_types,current_functional_sequence,current_alpha_basis,current_minimas,current_expansion_coeff)
      output_expansion+=current_y_output
  print(output_expansion)
  return output_expansion

#res = minimize(output_expansion,initial_guess,method='Nelder-Mead', tol=1e-6,bounds=bnds)
res=minimize(output_expansion,initial_guess,method='SLSQP')
print("Minimum found at:")
print(res.x)

