import yaml
import numpy as np
#from useful_functions import *
# GENERATE THE OPERATOR FILE FOR MCTDH --> EVERYTHING SHOULD COME FROM THE EXPANSION, THAT WAY YOU CAN GENERATE IT FROM THE EXPANSION SAVED IN THE FILE
def MCTDH_operator(filename_expansion,operator_file_name,name_of_the_expansion):
  expansion = yaml.load(open(filename_expansion,'r'), Loader=yaml.Loader)

  op_1='''OP_DEFINE-SECTION
title
MyTitle
end-title
end-op_define-section

PARAMETER-SECTION
mh  = 1.0, H-mass
mc  = 12.00,AMU
mo  = 15.9949146221,AMU
M11 = 1.0/mh+1.0/mc
'''

  print(op_1,file=open(operator_file_name, "a"))



  t=0
  print(expansion['full_list_of_terms'])
  for current_term in expansion['full_list_of_terms']:
    if expansion[str(current_term)]['status']!="Optimized": #Do not consider terms that were skipped
      print("We skipped term ",str(current_term))
    else:  
      v_shift=expansion[str(current_term)]['b2']
      Cq=expansion[str(current_term)]['w2']
      weights=expansion[str(current_term)]['w1']
      bq=expansion[str(current_term)]['b1']
      D=len(current_term)
      print("c",'T',t,'=',v_shift,file=open(operator_file_name, "a"),sep='')

      for i in range(len(Cq)):
        print("r",i,'T',t,"=",str(Cq[i]*np.exp(bq[i])).replace('e', 'd'),file=open(operator_file_name, "a"),sep='') #Multiplicative coefficients, Cq*exp(b)
        for j in range(D):
          print("w",i,"u",j,'T',t,"=",str(weights[j,i]).replace('e', 'd'),file=open(operator_file_name, "a"),sep='') #Weights arguments of the exponential w as 
    t+=1

  op_2='''end-parameter-section
LABELS-SECTION'''
  print(op_2,file=open(operator_file_name, "a"))

  t=0
  for current_term in expansion['full_list_of_terms']:
    #print(current_term)
    if expansion[str(current_term)]['status']!="Optimized": #Do not consider terms that were skipped
      print("We skipped term ",str(current_term))
    else:  
      v_shift=expansion[str(current_term)]['b2']
      Cq=expansion[str(current_term)]['w2']
      weights=expansion[str(current_term)]['w1']
      bq=expansion[str(current_term)]['b1']
      D=len(current_term)



      for i in range(len(Cq)):
        for j in range(D):
          print("q",i,"u",current_term[j],'T',t,"=exp[w",i,"u",j,'T',t,",0,0]",file=open(operator_file_name, "a"),sep='')
    t+=1

  print("end-labels-section",file=open(operator_file_name, "a")) 
  print("HAMILTONIAN-SECTION",file=open(operator_file_name, "a"))
  print("-----------------------------------------------",file=open(operator_file_name, "a"))
  print("modes       ",end='',file=open(operator_file_name, "a"))
  D=expansion['number_of_dimensions']
  for j in range(D):
    print("|  x_",j,'     ',end='',file=open(operator_file_name, "a"),sep='')
  print(" ",file=open(operator_file_name, "a"))
  print("-----------------------------------------------",file=open(operator_file_name, "a"))
 
  t=0
  for current_term in expansion['full_list_of_terms']:
    #print(current_term)
    if expansion[str(current_term)]['status']!="Optimized": #Do not consider terms that were skipped
      print("We skipped term ",str(current_term))
    else:
      v_shift=expansion[str(current_term)]['b2']
      Cq=expansion[str(current_term)]['w2']
      weights=expansion[str(current_term)]['w1']
      bq=expansion[str(current_term)]['b1']
      D=len(current_term)

      print("c",'T',t,"       ",end='',file=open(operator_file_name, "a"),sep='')
      for j in range(D):
        print("|    1 ",end='',file=open(operator_file_name, "a"))
      print(" ",file=open(operator_file_name, "a"))
      for i in range(len(Cq)):
        print("r",i,'T',t,end=' ',file=open(operator_file_name, "a"),sep='')
        for j in range(expansion['number_of_dimensions']):
          if j in current_term:
            print("|   q",i,"u",j,'T',t,end='',file=open(operator_file_name, "a"),sep='')
          else:
            print("|    1 ",end='',file=open(operator_file_name, "a"))
        print(" ",file=open(operator_file_name, "a"))
    t+=1
  print("-----------------------------------------------",file=open(operator_file_name, "a"))
  print("end-hamiltonian-section",file=open(operator_file_name, "a"))


  op_6='''end-operator
'''
  print(op_6,file=open(operator_file_name, "a"))



