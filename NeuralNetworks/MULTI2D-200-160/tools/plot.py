#!/usr/bin/env python

''' PLOT DATA in 3D FROM X,Y and E '''

import sys
import csv
import numpy as np
import matplotlib.pyplot as plt

if len(sys.argv) < 2:
    raise Exception("Syntax: plot.py two_column_file")

# Read CSV
csvFileName = sys.argv[1]
csvData = []
with open(csvFileName, 'r') as csvFile:
    csvReader = csv.reader(csvFile, delimiter=' ')
    for csvRow in csvReader:
        csvData.append(csvRow)

# Get X, Y, Z
csvData = np.array(csvData)
csvData = csvData.astype(np.float)
csvData = csvData[csvData[:, 2] < 0.5]
X, Y, Z = csvData[:, 0], csvData[:, 1], csvData[:, 2]

# Plot X,Y,Z
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_trisurf(X, Y, Z, color='white', edgecolors='grey', alpha=0.5)
ax.scatter(X, Y, Z, c='red')
plt.show()
