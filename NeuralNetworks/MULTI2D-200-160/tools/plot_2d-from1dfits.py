#!/usr/bin/env python

''' PLOT a 2D FORM FROM THE FITTED 1D POTENTIAL'''

import sys
sys.path.append('source/')

import yaml
import numpy as np
from potential import *


if len(sys.argv) < 3:
    raise Exception("Syntax: plot_2d-from1dfits.py expansion_file Coordinate(integer) batchsize(optional, default=100)")

filename_expansion = sys.argv[1]
expansion = yaml.load(open(filename_expansion,'r'), Loader=yaml.Loader)

PAIR = sys.argv[2]
if len(PAIR.split(',')) == 2:
  COORDINATE=[int(PAIR.split(',')[0]),int(PAIR.split(',')[1])]
else:
  raise Exception("Syntax error: the second argument asks for a pair of coordinates (exemple: 0,1)")

batchsize = 500  

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
#GENERATE A 1D TERM
str_term1='['+str(COORDINATE[0])+']'
str_term2='['+str(COORDINATE[1])+']'
print("Boundaries of the coordinates:", expansion['boundaries_coordinates'])
xy_min = [expansion['boundaries_coordinates'][COORDINATE[0]][0],expansion['boundaries_coordinates'][COORDINATE[1]][0]]
xy_max = [expansion['boundaries_coordinates'][COORDINATE[0]][1],expansion['boundaries_coordinates'][COORDINATE[1]][1]]
current_functional_sequence1=expansion[str_term1]["functional_sequence"]
current_functional_sequence2=expansion[str_term2]["functional_sequence"]
current_alpha_basis1=np.zeros(1)
current_alpha_basis2=np.zeros(1)
current_alpha_basis1[0]=expansion['alpha_basis'][COORDINATE[0]]
current_alpha_basis2[0]=expansion['alpha_basis'][COORDINATE[1]]
#current_alpha_basis=expansion[str_term]['alpha_basis']
current_minimas1=expansion[str_term1]['minimas']
current_minimas2=expansion[str_term2]['minimas']
current_coordinates_types1=expansion[str_term1]['coordinates_types']
current_coordinates_types2=expansion[str_term2]['coordinates_types']
current_expansion_coeff1=expansion[str_term1]['expansion_coeff']
current_expansion_coeff2=expansion[str_term2]['expansion_coeff']
points_on_grid=np.random.uniform(low=xy_min, high=xy_max, size=(batchsize,2))
y_out=np.zeros(batchsize)

t=0
for point in points_on_grid:
  y_out[t]=potential(point[0],1,current_coordinates_types1,current_functional_sequence1,current_alpha_basis1,current_minimas1,current_expansion_coeff1)[0]
  y_out[t]+=potential(point[1],1,current_coordinates_types2,current_functional_sequence2,current_alpha_basis2,current_minimas2,current_expansion_coeff2)[0]
  t+=1
x=points_on_grid[:,0]
y=points_on_grid[:,1]
z=y_out


fig = plt.figure()
ax = Axes3D(fig)
surf = ax.plot_trisurf(x, y, z, cmap=cm.jet, linewidth=0.1)
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()
