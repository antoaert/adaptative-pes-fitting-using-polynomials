#this should work just by giving the expansion and appropriate other defined functions.
from itertools import combinations,permutations
from functools import partial
import yaml
from constants import *
from basis_function import *
from potential import *
from training_poly import *
from useful_functions import *
import adaptive
from my_function_to_learn import *
from concurrent.futures import ProcessPoolExecutor
import math
import numpy as np
from scipy.optimize import curve_fit
from operator import itemgetter
'''This is the core of the program, it builds the expansion of the PES functional by computing abinitio points and fitting the
corresponding terms of the HDMR expansion that parametrize the form of the PES.'''

def construct_expansion(filename_expansion,DoISaveTheExpansion,IsThisARestart,DataBasePointsPath,SequentialOptimization,Verbose_Comment,local_energy_threshold,validation_frac,n_workers,temperature,learn_error):
  expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)
  os.system("touch ./stop")

  '''The procudure tries to take advantage of the particularity of an HDMR expansion. Indeed, it was observed that High dimensional data
  can often be described by lower order terms. At some points, the n-order term will become insignificant to the HDMR expansion. We try
  to exclude these terms beforehand. We use the "geometric mean" strategy defined BY EQ. (5) OF "Richter, F., Carbonniere, P., 
  & Pouchan, C. (2014). Toward linear scaling: Locality of potential energy surface coupling in valence coordinates. 
  International Journal of Quantum Chemistry, 114(20), 1401-1411." '''

#Threshold to skip a certain term based on the geometric mean of the significance of its parents
  skip_threshold=expansion['SkipThreshold']

  '''The program (should) handle a restart. Attention! The "mapped" data has to be provided (In the units and coordinates of the fit!,
  Not the coordinates used by the abinitio program!)'''

  if IsThisARestart:
    database_restart=np.genfromtxt(DataBasePointsPath,dtype=None,encoding=None)

  t=0
  #WE LOOP OVER THE TERMS OF THE EXPANSION TO CONSTRUCT IT
  for current_term in expansion['full_list_of_terms']:
    warning=0 #There is a warning that we don't want to see too many times.
    done=False #Optimize the alpha parameter only once for each coordinate.
    current_order=len(current_term) #NUMBER OF COORDINATES THAT CONCERN THIS TERM

    #TEST IF WE NEED TO SKIP THE TERM BASED ON USER INPUT
    if current_order>expansion['max_order']:
      expansion[str(current_term)]["status"]="Skip"
      print("The order of this term exceeds the maximum order of the expansion set by the user.")

    '''Since we fit the terms in increasing order of the expansion in an HDMR form, the n+1 order terms depend on the quality of 
    the n-order one(s). It is especially true that the primitive functions used for the fit are strictly equal to 0 if one of the coordinates
    is at equilibrium. Therefore, the n+1 order terms are unable to compensate for the inaccuracy of the n-order terms. It is then very difficult to
    obtain a fit RMSE as low for high order as compared to the low order ones. We then assign a RMSE threshold that is larger for higher order terms.
    The fit RMSE target defined by the user is multiplied by the number of parent (lower order) terms of the term being being optimized.'''

    numberoflowerorderterms=1
    if current_order!=1:
      numberoflowerorderterms=0
      for h in range(1,current_order):
        numberoflowerorderterms+=math.factorial(expansion['number_of_dimensions'])/(math.factorial(h)*math.factorial(expansion['number_of_dimensions']-h))
   
    '''With the a priori skip, we can skip terms that are assumed to be insignificant, without the need to compute points for this term.
    The GEO-DB scheme of Richter proved to be efficient and lead to minimal over-simplification according to their results.'''

    #A PRIORI SKIP: GEO-DB OF RICHTER Q-CHEM
    #We take the geomtric mean of the coupling_significance of all parent terms.
    if current_order>2: #The significance of the coupling is computated "only" starting with 2D terms
      #Find the parent terms:
      number_of_terms=0
      geometric_mean=1.0
      for order in range(0,current_order): #The loop goes up to current_order-1
        for combi in combinations(current_term,order):
          if list(combi)!=[]:
            parent_term=list(combi)
            if expansion[str(parent_term)]['status']=="Skip": #Do not consider terms that were skipped anyway
              break
            else:
              geometric_mean=geometric_mean*float(expansion[str(parent_term)]['coupling_significance'])
              number_of_terms+=1
      if number_of_terms!=0:
        geometric_mean=geometric_mean**(float(1./float(number_of_terms))) #nth rooth of the product of n significance_couplings
        if geometric_mean<skip_threshold:
          expansion[str(current_term)]["status"]="Skip"
          expansion[str(current_term)]['coupling_significance']=geometric_mean
          print("The dead branching criterion skipped term",str(current_term))
      else: #If it did not encounter another term, it means that they were all skipped, this one must therefore be skipped as well.
        expansion[str(current_term)]["status"]="Skip"
        print("All parent term of ",str(current_term)," were skipped, it is therefore skipped as well.")

    '''In case of a restart, the user could reduce the RMSE threshold. The term will not be improved if its RMSE meets already the new requirement.'''

    if (IsThisARestart) and (expansion[str(current_term)]['rmse_newpoints']/numberoflowerorderterms<expansion['rmse_target_newpoints']):
      expansion[str(current_term)]["status"]="Optimized"
      print("The RMSE of term ",str(current_term)," was already below the target for new points sampling during the last procedure run, coefficients will be read in the given file.")
  
    '''We also check the terms that were skipped due to the GEO-DB scheme and the coupling_significance threshold was changed.'''
  
    if expansion[str(current_term)]["status"]=="Skip":
      if expansion[str(current_term)]['coupling_significance']>expansion['SkipThreshold']: #if we changed the threshold for the skip, the term is reconsidered
        expansion[str(current_term)]["status"]=='NeverSeen'
      else: #If it is already below the threshold of significance, we don't reconsider it.
        print("We skipped term:"+str(current_term))
    elif expansion[str(current_term)]["status"]=="Optimized":
      print("The coefficients of term "+str(current_term)+" will be read in the given file.")
    else:
      print("Working on term:"+str(current_term))
      expansion[str(current_term)]['status']="Ongoing"
      current_w2=expansion[str(current_term)]['w2']
      current_b2=expansion[str(current_term)]['b2']
      current_w1=expansion[str(current_term)]['w1']
      current_b1=expansion[str(current_term)]['b2']
      if DoISaveTheExpansion: #Keep the current state of the expansion in a file
        file_expansion = open(filename_expansion, "w")
        yaml.dump(expansion, file_expansion)
        file_expansion.close()
  

      '''We decided that the non-linear coefficients (except for pure polynomials) need to be the same for all terms in the expansion.
      They are kept in the dictionnary that defines the expansion and are read from there when needed.'''


      '''We read user-defined threshold and informations from the dictionary.'''

      current_minimas=expansion[str(current_term)]['minimas']
      current_coordinates_types=expansion[str(current_term)]['coordinates_types']
      boundaries_coordinates=expansion['boundaries_coordinates']
      local_boundaries=expansion['local_bounds']

      '''The procedure will compute ab-initio points until it meets a convergence criterion. To stop computing points, it relies on a 
      validation data set. The RMSE of the fit (that has not seen the validation set) with respect to the validation data must be lower 
      than the user-defined value. AND the validation set must be at least a user-defined fraction of the fitting data set.
      If these criteria are met, the the need_more_points variable is set to False.'''

      need_more_points=True
      iter=0


      '''The sampling of the data points is handled by the 'adaptive' library (see https://adaptive.readthedocs.io/en/latest/ for details).
      Using the library would require to define a function to be learned (here the energy with respect to the coordinates) for each term of 
      the HDMR expansion. Instead we use the partial function of python. The adaptive learner is defined here. There is a 1D version and 
      ND version of the learner, details are in the documentation of the library.
      
      The learner is given coordinates boundaries to start with, this was defined by the user.'''

      #WE NEED TO INITIATE THE LEARNER OF THE TERM OUTSIDE THE LOOP OR IT WILL HAVE TO RECOMPUTE EVERYTHING AT EACH ITER
      #USE OF PARTIAL : https://www.geeksforgeeks.org/partial-functions-python/
      if current_order==1:
#        learner = adaptive.Learner1D(partial(my_function_to_learn,current_term=current_term,minimum_coordinates=expansion['minimum_coordinates']),(boundaries_coordinates[current_term[0]][0], boundaries_coordinates[current_term[0]][1]))
        _learner = adaptive.Learner1D(partial(my_function_to_learn,current_term=current_term,minimum_coordinates=expansion['minimum_coordinates'],temperature=temperature),(local_boundaries[current_term[0]][0], local_boundaries[current_term[0]][1]))
        learner = adaptive.DataSaver(_learner, arg_picker=itemgetter('error'))

      elif current_order>=2:
      #BASED ON THE TERM, WE NEED THE APPROPRIATE BOUNDS
        current_bounds=[]
        for i in current_term:
          current_bounds.append(boundaries_coordinates[i])
        _learner = adaptive.LearnerND(partial(my_function_to_learn,current_term=current_term,minimum_coordinates=expansion['minimum_coordinates'],temperature=temperature), bounds=current_bounds)
        learner= adaptive.DataSaver(_learner, arg_picker=itemgetter('error'))
      
      current_x_train_t=[]
      current_y_target_t=[]
      while need_more_points:   #-> Adapt a loss function to exlcude high energy regions? We used a workaround.
        iter+=1
        #if iter==1:#GENERATE THE FIRST POINTS FOR THE TERM (THE DATA IS TERM SPECIFIC!)  
        old_y_target=[]
        if iter>1:
          old_x_train=current_x_train
          old_y_target=current_y_target #THIS WILL HAVE THE EFFECT OF THE PARENT IF APPLICABLE!


        '''We define the goal of the sampling procedure, this will be adapted as we go forward in the learning process.
        The goal is defined either by a minimum number of points to be sample:
        $npoints_goal = lambda l: l.npoints >= 2*iter
        
        Or one can also define the maximal width of the feature to be leaned. This has more sense when your data has peaks. This goal is then
        the maximum width of the peaks that you want to find. One need to use the two lines :
        $a=(boundaries_coordinates[current_term[0]][1]-boundaries_coordinates[current_term[0]][0])/(float(iter)/10.)
        $nloss= lambda l: l.loss()<a'''


        if current_order==1:
          npoints_goal = lambda l: l.npoints >= n_workers*iter*2
          #a=(boundaries_coordinates[current_term[0]][1]-boundaries_coordinates[current_term[0]][0])/(float(iter)/10.)
          #nloss= lambda l: l.loss()<a

          '''In case of a restart, we need to feed the learner with the appropriate points.'''
          #TO BE ADDED
          if len(old_y_target)>1:
#            print("coucou",current_x_train,current_y_target)
            _learner = adaptive.Learner1D(partial(my_function_to_learn,current_term=current_term,minimum_coordinates=expansion['minimum_coordinates'],temperature=temperature),(local_boundaries[current_term[0]][0], local_boundaries[current_term[0]][1]))
            learner = adaptive.DataSaver(_learner, arg_picker=itemgetter('error'))
            kk=0
#            print('tell',current_x_train_t)
#            print(current_y_target_t) 
            for point in current_x_train_t:
              allpoints_output,dump,dump=potential(np.transpose(point),current_w1,current_b1,current_w2,current_b2)
              error=allpoints_output-current_y_target_t[kk]
#              print('(',point,',',error,')')
              learner.tell(point,{'energ': current_y_target_t[kk],'energ_filter': current_y_filter[kk], 'error': error})
#              print("Told the learner that:",point,"->",error)
              kk+=1
          #SAMPLING 1D happens here:
          if expansion['VerboseComment']:
            print("Sampling of new points")

          '''We can define the executor of the sampling to use multiple cores, this is experimental at the moment !!!'''

          executor = ProcessPoolExecutor(max_workers=n_workers)
          adaptive.BlockingRunner(learner,goal=npoints_goal,executor=executor)
#          adaptive.BlockingRunner(learner,goal=nloss,executor=executor)
          #RESULTS:
          current_x_train=np.copy(list(learner.data.keys()))
#          print('results',current_x_train)
          current_x_train_t=np.copy(current_x_train)
          current_dict=np.array(list(learner.extra_data.items()))
#          print('dictionary',current_dict)
          current_y_target=np.zeros(len(current_dict))
          current_y_filter=np.zeros(len(current_dict))
          for i in range(len(current_dict)):
            current_y_target[i]=current_dict[i][1]['energ']
            current_y_filter[i]=current_dict[i][1]['energ_filter']
          current_y_target_t=np.copy(current_y_target)
#          print(current_y_target_t)

          '''The points that are above the threshold do not need to be given to the fitting procedure.
          For the 1D term, the user has to define local energy bounds. It might be useful to converge the 1D terms up to a larger energy
          as compared to the ND terms (N>1) since it will ease the fitting of the ND terms and is usually not costly.'''

          #WE HAVE TO REMOVE POINTS THAT ARE ABOVE THE THRESHOLD
          if expansion['EnergyThreshold']:
            energy_threshold=expansion['Threshold(cm-1)']/wavenumber_per_ua
            number_of_deletion=len(np.where(current_y_target>local_energy_threshold)[0])
             
            if number_of_deletion>0:
              current_x_train=np.delete(current_x_train, np.where(current_y_filter>local_energy_threshold)[0], axis=0)
              current_y_target=np.delete(current_y_target, np.where(current_y_filter>local_energy_threshold)[0], axis=0)
            if expansion['VerboseComment']:
              print("Deleted ", number_of_deletion," points above the Threshold")
              print("Current ytarget has ",len(current_y_target)," points")
              print("Deleted len",len(current_x_train),len(list(learner.data.values())))  

          '''One of the tricky things to define in the expansion is the value of the non-linear coefficient of the primitive functions. 
          We want to avoid primitives whose values are too big outside of the boundaries, therefore we minimize the non-linear parameter such that
          1)we minimize the error of the fit
          2)the linear coefficient (of the expansion) are as low as possible
          We achieve this by using a non-linear optimization procedure based on the LM algorithm. 
          And the initial value of the linear coefficients are taken small to lead to the solution that has hopefully the smallest 
          linear coefficients.
          The 'harm' and 'morse' function that are optimized are in the basis_function.py file.''' 


        elif current_order>=2:
          ''' All above steps are taken for the higher order terms as well, except the optimization of the non-linear coefficients.'''
          npoints_goal = lambda l: l.npoints > iter*n_workers #This sets a number of points to compute
          a=(boundaries_coordinates[current_term[0]][1]-boundaries_coordinates[current_term[0]][0])/(0.25*float(iter))
          nloss= lambda l: l.loss()<a
          #npoints_goal = lambda l: l.npoints >= 2*iter
          #TELL THE LEARNER ABOUT THE POINTS IN THE DATABASE
          executor = ProcessPoolExecutor(max_workers=n_workers)
          if len(old_y_target)>1:
#            print("coucou",current_x_train,current_y_target)
            _learner = adaptive.LearnerND(partial(my_function_to_learn,current_term=current_term,minimum_coordinates=expansion['minimum_coordinates'],temperature=temperature), bounds=current_bounds)
            learner = adaptive.DataSaver(_learner, arg_picker=itemgetter('error'))
            kk=0
#            print('tell',current_x_train_t)
#            print(current_y_target_t) 
            for point in current_x_train_t:
              allpoints_output,dump,dump=potential(np.transpose(point),current_w1,current_b1,current_w2,current_b2)
              error=allpoints_output-current_y_target_t[kk]
#              print('point',point)
#              print(tuple(point),{'energ': current_y_target_t[kk], 'energ_filter': current_y_filter[kk], 'error': float(error)})
              learner.tell(tuple(point),{'energ': current_y_target_t[kk], 'energ_filter': current_y_filter[kk], 'error': float(error)})
#              print("Told the learner that:",point,"->",error)
              kk+=1

          adaptive.BlockingRunner(learner,goal=npoints_goal,executor=executor)
          #RESULTS:
          current_x_train=np.copy(list(learner.data.keys()))
#          print('results',current_x_train)
          current_x_train_t=np.copy(current_x_train)
          current_dict=np.array(list(learner.extra_data.items()),dtype=object)
#          print('dictionary',current_dict)
          current_y_target=np.zeros(len(current_dict))
          current_y_filter=np.zeros(len(current_dict))
          for i in range(len(current_dict)):
            current_y_target[i]=current_dict[i][1]['energ']
            current_y_filter[i]=current_dict[i][1]['energ_filter']
          current_y_target_t=np.copy(current_y_target)
#          print(current_y_target_t)

          #WE HAVE TO REMOVE POINTS THAT ARE ABOVE THE THRESHOLD
          
          if expansion['EnergyThreshold']:
            energy_threshold=expansion['Threshold(cm-1)']/wavenumber_per_ua
            number_of_deletion=len(np.where(current_y_target>energy_threshold))
            if number_of_deletion>0:
              if expansion['VerboseComment']:
                print("Deleted point(s) above the threshold")
              current_x_train=np.delete(current_x_train, np.where(current_y_filter>energy_threshold)[0], axis=0)
              current_y_target=np.delete(current_y_target, np.where(current_y_filter>energy_threshold)[0], axis=0)
            if expansion['VerboseComment']:
              print("Deleted ", number_of_deletion," points above the Threshold")
              print("Current ytarget has ",len(current_y_target)," points")

        number_of_new_points=len(current_y_target)-len(old_y_target)

      #THE DATA THAT NEEDS TO BE FITTED IS THE DIFFERENCE WITH THE PARENT TERMS
      #HOW TO GET PARENT TERMS ?
        if True==True:#number_of_new_points>0 or IsThisARestart:
          '''The higher (than one) order terms are ``additions" to the lower order terms. Therefore the ``parents" effect has to be
          taken into account.'''
          if current_order!=1: #THERE ARE NO PARENTS FOR THE 1D cuts
            #We loop over the parent_terms
            parents_number_of_points=0
            raw_current_y_target=np.copy(current_y_target)
            for order in range(1,current_order): #The loop goes up to current_order-1
              for combi in combinations(current_term,order):
                if list(combi)!=[]:
                  parent_term=list(combi)
                  if expansion[str(parent_term)]['status']!="Optimized": #Do not consider terms that were skipped
                    continue 
                  else:
#                    print("Take into account effect of",parent_term)
                    parent_order=len(parent_term)
                    parent_w2=expansion[str(parent_term)]['w2'] #what are the parameters of the parent terms (of lower order) 
                    parent_b2=expansion[str(parent_term)]['b2']
                    parent_w1=expansion[str(parent_term)]['w1']
                    parent_b1=expansion[str(parent_term)]['b1']
                    parent_minimas=expansion[str(parent_term)]['minimas']
                    parent_coordinates_types=expansion[str(parent_term)]['coordinates_types']
                    parent_number_of_points=expansion[str(parent_term)]['number_of_points']
                    parents_number_of_points+=int(parent_number_of_points)
                    t=0
                    for point in current_x_train:
                      #Need to extract the coordinates that belong to the parent term
                      parent_point=np.zeros(parent_order) #As long as the order of the term!
                      for i in range(parent_order):
                        index=np.where(np.array(current_term)==parent_term[i])
                        index=int(index[0][0])
                        parent_point[i]=point[index]
                      #FOR THAT PARENT POINT (ASSOCIATED TO PARENT TERM), WE COMPUTE THE POTENTIAL OUTPUT
                      parent_y_output,dump,dump=potential(parent_point,parent_w1,parent_b1,parent_w2,parent_b2)
                      #We simply dump the derivative here, there is no overhead for its calculation anyways.
                      current_y_target[t]-=float(parent_y_output) #WE SUBSTRACT THE OUPUT OF THE PARENT TERM CONTRIBUTION FROM THE CURRENT TERM POINT OUTPUT 
                      t+=1
        ##END EFFECT OF THE PARENTS ON THE FIRST POINTS 
#            print('356optim', current_y_target) 
        #STARTING WITH 2D TERMS, WE COMPUTE THE COUPLING SIGNIFICANCE SUCH AS DEFINED BY EQ. (5) OF "Richter, F., Carbonniere, P., & Pouchan, C. (2014). Toward linear scaling: Locality of potential energy surface coupling in valence coordinates. International Journal of Quantum Chemistry, 114(20), 1401-1411."
            if iter !=1 and len(current_y_target)>0 and parents_number_of_points>0:
              coupling_significance=np.sqrt((1./parents_number_of_points)*np.sum((current_y_target/raw_current_y_target)**2.))
              expansion[str(current_term)]['coupling_significance']=coupling_significance
              if expansion['VerboseComment']:
                print("Coupling significance:",coupling_significance)
            #If the coupling significance is too low, we just skip this term, it accounts for too little to the potential energy.
            #The term will be dumped if it falls below the threshold, only if we have computed enough points (hopefully)
            #This number is set to (2*(order+1)!) where (x)! is the factorial of x.
              if (len(current_y_target)>2.*math.factorial(current_order+1)) and (expansion[str(current_term)]['coupling_significance']<expansion['SkipThreshold']):
                expansion[str(current_term)]['status']='Skip'
                construction_of_term=False
                need_more_points=False
                if DoISaveTheExpansion: #Keep the current state of the expansion in a file
                  file_expansion = open(filename_expansion, "w")
                  yaml.dump(expansion, file_expansion)
                  file_expansion.close()


          '''If we already have optimized expansion coefficients, then we compute the error of the fit on the new points that were computed.'''
      #WHAT IS THE ERROR GIVEN THE NEW POINTS?
      # IF THE ERROR IS BELOW THE THRESHOLD, WE CAN STOP COMPUTING POINTS FOR THIS TERM
          if (iter!=1) and (len(current_y_target)>1) and number_of_new_points>0: #DOES NOT APPLY TO THE FIRST ITERATION
            #COMPUTE THE OUTPUT OF THE CURRENT EXPANSION FOR ALL THE X_TRAIN
            error_newpoints=0.0
            gap=np.zeros(len(current_y_target))
            current_w2=expansion[str(current_term)]['w2']
            current_b2=expansion[str(current_term)]['b2']
            current_w1=expansion[str(current_term)]['w1']
            current_b1=expansion[str(current_term)]['b1']
            t=0
            for point in current_x_train:
              allpoints_output,dump,dump=potential(point,current_w1,current_b1,current_w2,current_b2)
              gap[t]=(allpoints_output-current_y_target[t])
              error_newpoints+=float(gap[t])**2.
              t+=1
            rmse_newpoints=np.sqrt(1./(len(current_y_target))*error_newpoints)
            expansion[str(current_term)]['rmse_newpoints']=rmse_newpoints #We keep this information in the dictionary for each term. Can be used in a restart.
            if expansion['VerboseComment'] and (number_of_new_points>0):
              print("ITER:",iter,"ABSOLUTE ERROR OF THE EXPANSION ON NEW POINTS:",error_newpoints)
            print("RSME_NEWPOINTS",rmse_newpoints," TARGET:",expansion['rmse_target_newpoints']*numberoflowerorderterms)
            if not os.path.isfile('./stop'):
              print("You asked me to stop maniac")
              exit()

            '''The fitting will be done below, we also asses if the addition of fitting coefficients need to stop, if we need more points or if
            the term is optimized.'''

        #WE DETERMINE THE EXPANSION COEFFICIENTS
          construction_of_term=False
          if (len(current_y_target!=0)) and (expansion[str(current_term)]['status']!='Skip') and (len(current_y_target)>2): #No need to consider the fit of an empty training set
                                                                                              #No need to consider terms that are skipped (by the coupling significance test).
            if iter==1 or(IsThisARestart and iter ==2) : #We need to construct the first time
              construction_of_term=True
            if iter !=1:
              #print(number_of_new_points)
              if number_of_new_points>0: 
                if (rmse_newpoints/numberoflowerorderterms<expansion['rmse_target_newpoints']): #If we meet the target of rmse for new points.

                  '''We want the total set of data (training + validation) to have a certain fraction of validation with respect to the trainint (or
                  fitting) set. This fraction is defined by the user.'''
#                  print(len(current_y_target),(1.+validation_frac)*training_set_size, "trset size",training_set_size)
                  if (len(current_y_target)>(1.+validation_frac)*training_set_size):
                    if (len(current_y_target)>20*current_order):
                      need_more_points=False
                  else:
                    need_more_points=True
                  construction_of_term=False
                  expansion[str(current_term)]['status']="Optimized"
                  expansion[str(current_term)]['number_of_points']=str(len(current_y_target))
                  expansion[str(current_term)]['fit_points']=str(training_set_size)
                  expansion[str(current_term)]['valid_points']=str(len(current_y_target)-training_set_size)
                  print("We had to compute ",str(len(current_y_target)),"points for this term.")
                  if DoISaveTheExpansion: #Keep the current state of the expansion in a file
                    file_expansion = open(filename_expansion, "w") #w if json
                    yaml.dump(expansion, file_expansion)
                    file_expansion.close()
                else:
                  construction_of_term=True
              elif number_of_new_points==0: #No need to consider the fitting if we have no new points to work with.
                construction_of_term=False
                need_more_points=True
                iter+=1


          while construction_of_term:
            current_w2=expansion[str(current_term)]['w2']
            current_b2=expansion[str(current_term)]['b2']
            current_w1=expansion[str(current_term)]['w1']
            current_b1=expansion[str(current_term)]['b1']
          #-> Maybe a better starting guess for the expansion coeff could be nice
            if iter>3 and not(IsThisARestart):
              if (len(current_x_train)-len(old_x_train))<0:
                print("Your training set is reducing in size !?!?")
            y_training=np.copy(current_y_target)
            w2_list=[[]]*5
            b2_list=[[]]*5
            w1_list=[[]]*5
            b1_list=[[]]*5
            rmse_fit=[[]]*5
            for i in range(5):
              y_training=np.copy(current_y_target)
              w2_list[i],b2_list[i],w1_list[i],b1_list[i]=training_network(current_x_train,current_y_target,len(current_w2),len(current_term),2000,True,verbose=False,minibatch=1)
              error_fit=0.0
              gap=np.zeros(len(current_y_target))
              t=0 
              for point in current_x_train:
                allpoints_output,dump,dump=potential(point,w1_list[i],b1_list[i],w2_list[i],b2_list[i])
                gap[t]=(allpoints_output-current_y_target[t])
                error_fit+=float(gap[t])**2.
                t+=1
              rmse_fit[i]=np.sqrt(1./(len(current_y_target))*error_fit)
            try:
                k=np.where(np.array(rmse_fit)==min(rmse_fit))[0][0]
            except:
                k=1
            w2=w2_list[k]
            b2=b2_list[k]
            w1=w1_list[k]
            b1=b1_list[k] 

            rmse_fit=rmse_fit[k]
            training_set_size=len(y_training)
              #Adapt the expansion_coeff in the dictionary
            expansion[str(current_term)]['w2']=w2
            expansion[str(current_term)]['b2']=b2
            expansion[str(current_term)]['w1']=w1
            expansion[str(current_term)]['b1']=b1

            if DoISaveTheExpansion: #Keep the current state of the expansion in a file
              file_expansion = open(filename_expansion, "w") #w if json
              yaml.dump(expansion, file_expansion)
              file_expansion.close()
            print("RMSE_FIT",rmse_fit,"number of neurons",len(w2),"target number",len(current_y_target))
          
          #Now that we have a new fit, 

            '''Once the RMSE of the fit is good enough, we stop adding coefficients (or primitive basis functions).'''

            if iter==1 and (1.0/0.8*rmse_fit/numberoflowerorderterms<expansion['rmse_target_fit']or (len(current_y_target)<=4*len(w2))): #We are good to go, compute new points to test the quality of this new fit
              need_more_points=True
              iter+=1
              construction_of_term=False
            elif iter==1 and (1.0/0.8*rmse_fit/numberoflowerorderterms>expansion['rmse_target_fit']): #We need a new term.
              need_more_points=False
              construction_of_term=True 
            
            elif iter==1 and (len(current_y_target)<len(w2)):
              print("You certainly have a problem with your set of basis functions as it needs more fitting coefficients than it has points")
            elif (rmse_newpoints/numberoflowerorderterms<expansion['rmse_target_newpoints']) and (1.0/0.8*rmse_fit/numberoflowerorderterms<expansion['rmse_target_fit']):
              construction_of_term=False
              need_more_points=True
              expansion[str(current_term)]['status']="Optimized"
              expansion[str(current_term)]['number_of_points']=str(len(current_y_target))
              if DoISaveTheExpansion: #Keep the current state of the expansion in a file
                file_expansion = open(filename_expansion, "w") #w if json
                yaml.dump(expansion, file_expansion)
                file_expansion.close()

           

#            elif iter!=1 and (rmse_fit>rmse_newpoints and rmse_fit<1.5*rmse_newpoints and len(new_expansion_coeff)>(current_order+1)**2):
#              '''If the fit is very bad, we compute more points to help the procedure optimizing the coefficients.'''
#              construction_of_term=False
#              need_more_points=True
#              iter+=1
            elif iter!=1 and ((rmse_fit/(0.8*numberoflowerorderterms)<expansion['rmse_target_fit']) or (len(current_y_target)<=4*len(w2))):
                                                             #Stop building if we have more parameters than data points
#              print((rmse_fit/(0.8),"compared to", expansion['rmse_target_fit']*numberoflowerorderterms))
              construction_of_term=False
              need_more_points=True
              iter+=1

            if construction_of_term:
              need_new_f_s=True
              while need_new_f_s:
                current_w1=np.zeros((len(current_term),len(w2)+1))
                current_b1=np.zeros((len(w2)+1))
                current_w2=np.zeros((len(w2)+1))
                current_b2=np.zeros((1))
                expansion[str(current_term)]['w2']=current_w2
                expansion[str(current_term)]['b2']=current_b2
                expansion[str(current_term)]['w1']=current_w1
                expansion[str(current_term)]['b1']=current_b1
                need_new_f_s=False 
               
