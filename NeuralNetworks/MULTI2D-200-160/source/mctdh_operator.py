import yaml
from useful_functions import *
# GENERATE THE OPERATOR FILE FOR MCTDH --> EVERYTHING SHOULD COME FROM THE EXPANSION, THAT WAY YOU CAN GENERATE IT FROM THE EXPANSION SAVED IN THE FILE
def MCTDH_operator(filename_expansion,operator_file_name,name_of_the_expansion):
  expansion = yaml.load(open(filename_expansion,'r'))

  op_1='''OP_DEFINE-SECTION
title
HFCO in valence coordinates. r1=r(C-H), r2=r(C-F), r3=r(C-O), beta1=(O-C-H), beta2=(O-C-F)
phi = angle (O-C-F) and (O-C-H).
end-title
end-op_define-section

PARAMETER-SECTION
mh  = 1.0, H-mass
mc  = 12.00,AMU
mo  = 15.9949146221,AMU
mf  = 18.99840320,AMU
M11 = 1.0/mh+1.0/mc
M22 = 1.0/mf+1.0/mc
M33 = 1.0/mo+1.0/mc
Mu  = 1.0/mc            # Mu = Mij; i neq j
'''

  op_2='''end-parameter-section
LABELS-SECTION'''
  print(op_1,file=open(operator_file_name, "a"))
  print(op_2,file=open(operator_file_name, "a"))





  #WE CAN GO THROUGH ALL TERMS AND ALL ORDERS TO CONSTRUCT ALL THE LABELS THAT WE NEED
  labels_needed=[[] for i in range(expansion['number_of_dimensions'])]
  for current_term in expansion['full_list_of_terms']:
    #print(current_term)
    if expansion[str(current_term)]['status']=="Skip": #Do not consider terms that were skipped
      print("We skipped term ",str(current_term))
      break
    current_functional_sequence=expansion[str(current_term)]['functional_sequence'] #what is the sequence of functionals for that term
    coordinates_types=expansion['type_of_coordinates']
    current_order=len(current_term)

    for i in range(current_order):
      current_list=labels_needed[current_term[i]]
      for functional in current_functional_sequence:
        current_list.append(functional[i])
  #REMOVE THE DUPLICATES THAT WERE FOUND AND ADD ALL THE LABELS NEEDED INTO THE OPERATOR FILE
  for i in range(expansion['number_of_dimensions']):
    string_term='['+str(i)+']'
    labels_needed[i]=remove_duplicates(labels_needed[i])
    for j in range(len(labels_needed[i])):
      label_string=''
      label_string+=str(str(name_of_the_expansion)+'T'+str(i+1)+'OR'+str(labels_needed[i][j])+'=').replace('-','M') #MCTDH DOES NOT ACCEPT '-' IN THE NAME OF A LABEL
      if coordinates_types[i]=='stretch':
        label_string+='exp1['+str('-'+'{0:1.6f}'.format(expansion['alpha_basis'][i]).replace('e', 'd'))+','+str('{0:1.6f}'.format(expansion[string_term]['minimas'][0]).replace('e', 'd'))+']^'+str(int(labels_needed[i][j]))
      elif coordinates_types[i]=='angle':
        label_string+='q['+str('{0:1.6f}'.format(expansion[string_term]['minimas'][0]).replace('e', 'd'))+','+str('{0:1.6f}'.format(expansion['alpha_basis'][i]).replace('e', 'd'))+']^'+str(int(labels_needed[i][j]))
      elif coordinates_types[i]=='torsion':
        if (labels_needed[i][j])<0.:
          label_string+='sin['+str(abs(int(labels_needed[i][j])))+','+str('{0:1.6f}'.format(expansion[string_term]['minimas'][0]).replace('e', 'd'))+']'
        else:
          label_string+='cosS['+str(int(labels_needed[i][j]))+','+str('{0:1.6f}'.format(expansion[string_term]['minimas'][0]).replace('e', 'd'))+',1.0d0]'
      print(label_string,file=open(operator_file_name, "a"))

  op_3='''end-labels-section
HAMILTONIAN-SECTION
-----------------------------------
modes     |  x_1  |  x_2  | x_3
-----------------------------------
''' #ADD THE KINETIC ENERGY OPERATOR HERE !!!!!!!!!!!
  print(op_3,file=open(operator_file_name, "a"))

  #GO THROUGH THE EXPANSION AND CONSTRUCT THE SUM OF PRODUCTS WITH THE OPTIMIZED COEFFICIENTS
  for current_term in expansion['full_list_of_terms']:
    #print(current_term)
    if expansion[str(current_term)]['status']!="Optimized": #Do not consider terms that were skipped or never seen
      break
    current_order=len(current_term)
    current_functional_sequence=expansion[str(current_term)]['functional_sequence'] #what is the sequence of functionals for that term
    current_expansion_coeff=expansion[str(current_term)]['expansion_coeff']
    for i in range(len(current_expansion_coeff)):
      string_element_of_expansion=''
      string_element_of_expansion+=str(current_expansion_coeff[i]).replace('e', 'd')
      for j in range(len(current_term)):
        string_element_of_expansion+='   |'+str(int(current_term[j])+1)+'   '+str(str(name_of_the_expansion)+'T'+str(int(current_term[j])+1)+'OR'+str(current_functional_sequence[i][j])).replace('-','M')
      print(string_element_of_expansion,file=open(operator_file_name, "a"))
  

  op_4='''-----------------------------------
end-hamiltonian-section'''
  print(op_4,file=open(operator_file_name, "a"))

  op_5='''-----------------------------------
modes     |  x_1  |  x_2   | x_3
-----------------------------------'''

  #Add all the 1D terms for WF initialization
  t=1
  for current_term in expansion['full_list_of_terms']:
    if (len(current_term)==1) and (expansion[str(current_term)]['status']=="Optimized"):
      print("hamiltonian-section_cut."+str(t),file=open(operator_file_name, "a"))
      print(op_5,file=open(operator_file_name, "a"))
      current_order=len(current_term)
      current_functional_sequence=expansion[str(current_term)]['functional_sequence'] #what is the sequence of functionals for that term
      current_expansion_coeff=expansion[str(current_term)]['expansion_coeff']
      for i in range(len(current_expansion_coeff)):
        string_element_of_expansion=''
        string_element_of_expansion+=str(current_expansion_coeff[i]).replace('e', 'd')
        for j in range(len(current_term)):
          string_element_of_expansion+='   |'+str(int(current_term[j])+1)+'   '+str(str(name_of_the_expansion)+'T'+str(int(current_term[j])+1)+'OR'+str(current_functional_sequence[i][j])).replace('-','M')
        print(string_element_of_expansion,file=open(operator_file_name, "a"))
      print('end-hamiltonian-section',file=open(operator_file_name, "a"))
      t+=1





  op_6='''end-operator
'''
  print(op_6,file=open(operator_file_name, "a"))
