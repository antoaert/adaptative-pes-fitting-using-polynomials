import numpy as np

def apply_exp_layer(x_in,w,b):
    z=np.dot(x_in,w)+b
    return z,np.exp(z)    #net,output,slope

def apply_linear_layer(x_in,w,b):
    z=np.dot(x_in,w)+b
    return z


def potential(x_in,w1,b1,w2,b2):
    z,y_1=apply_exp_layer(x_in,w1,b1)
    y_out=apply_linear_layer(y_1,w2,b2)
    return (y_out,y_1,z)
