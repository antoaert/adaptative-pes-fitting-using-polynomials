import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from functools import partial
import tempfile
import shutil
from tabulate import tabulate
import numpy as np
from potential import *
from scipy.optimize import curve_fit
from useful_functions import *

def training_network(x_full,y_full,num_neurons,num_dimensions,epochs,NGW,**kwargs):
    verbose=kwargs.get('verbose', False)
    string_network=kwargs.get('name'," ")
    batch_number=kwargs.get('minibatch', 1)
    target_error=0.00000005
    x_1=np.zeros((len(x_full),num_dimensions))
    D=num_dimensions
    t=0
    for point in x_full:
        if num_dimensions==1:
            point=[point]
        for i in range(num_dimensions):
           x_1[t,i]=point[i] 
        t+=1
    x_full=x_1
#    print(x_full)
    boundariesx=[-1.,1.]
    boundariese=[0,1.]
   #We select part of the training set for the test set.
    unison_shuffled_copies(x_full,y_full)
    test_size=int(np.floor(0.3*len(x_full[:])))
    if test_size==0:
      test_size=1
    x_train,x_test=x_full[test_size:],x_full[:test_size]
    y_target,y_test=y_full[test_size:],y_full[:test_size] 
#NORMALIZATION OF DATA FOR FITTING
    (x_train,y_target,x_test,y_test,energy_max,energy_min,x_min,x_max)=scaling_datasets2(num_dimensions,x_train,y_target,x_test,y_test,boundariesx,boundariese)
    x_train_original=x_train
    y_target_original=y_target

    if NGW==False:
        w2 = kwargs.get('w2', None)
        w1 = kwargs.get('w1', None)
        b2 = kwargs.get('b2', None)
        b1 = kwargs.get('b1', None)
        w2_init=w2
        w1_init=w1
        b2_init=b2
        b1_init=b1
    mu_inc=1.1                       #This is the factor by which "mu" is multiplied if the error is increasing after a few consecutive interations.
                                #If this factor is too small, the algorithm is no longer converging monotonically. 
                                #If this factor is too big, the algorithm 1) is very slow, 2) might be stuck at a certain performance.
    mu_dec=0.80
    mu=1.0 #First value of mu
    epoch=0
    shuffle=True
    restart=False
    old_error=10.
    old_error_test=1000000.
    m=0
    p=0
    g=0
    q=0
    prop=0
    while epoch<epochs :
        if restart == True or epoch==0 : #Can be used to avoid "NAN" errors, if the networks value blows up. This can easily happens as the network is built from exponential neurons.
            epoch=0
            if NGW==True:
                threshold=0.00000001
                ######## NGUYEN - WIDROW ##########
                H=0.2*0.7*num_neurons**(1./D)
                w1=np.random.uniform(low=-1.0*H,high=1.0*H,size=[num_dimensions,num_neurons])
                b1=np.random.uniform(low=-1.0,high=1.0,size=[num_neurons])
                w2=np.zeros((num_neurons))
                w2.fill(1./num_neurons)
                b2=np.zeros((1))
                b2.fill(-1.)
                ######## NGUYEN - WIDROW #END######
            else:
                w1=w1_init
                w2=w2_init
                b1=b1_init
                b2=b2_init

#        if batch_number!= 1:
#            number_of_elements_in_batch=int(np.floor(len(x_train_original[:,-1])/batch_number))
#            x_train=x_train_original[q*number_of_elements_in_batch:(q+1)*number_of_elements_in_batch]
#            y_target=y_target_original[q*number_of_elements_in_batch:(q+1)*number_of_elements_in_batch] 
#            print(q)
#            q+=1
#            if q>batch_number:
#                q=0


        y_out,y_1,z=potential(x_train,w1,b1,w2,b2)
        y_1=y_1.T
        z=z.T

        error=(np.square(y_target-y_out))   #This is a vector (1D problem) or a matrix (ND problem). For each point, it stores the error.
                                            #The error matrix (vector) should be built for all training training point in order to compute 
                                            #the "gap" vector used in the LM algorithm
        gap=y_target-y_out

        y_net_test,trash,trash1=potential(x_test,w1,b1,w2,b2)
        error_test=(np.square(y_test-y_net_test))        #The followed error is based on the "test" set.
        test=np.sqrt(1./len(y_test)*np.sum(np.square(y_test-y_net_test)))

        error_rmse=np.sqrt(1./len(x_train)*np.sum(error))

        if verbose==True:
            print('error_rmse_train',error_rmse)
            print('error_rmse_test',test)
        jacobian=np.zeros((num_neurons*(D+2)+1,len(x_train)))
        B=np.multiply(w2.T[:,np.newaxis],y_1)
        gap=gap
        for k in range(D):    #derivative of network output with respect to w1
            for j in range(num_neurons):
                jacobian[k*num_neurons+j,:]=B[j,:]*x_train.T[k,:]*-1.#*gap
        for j in range(num_neurons):
            jacobian[(D-1)*num_neurons+num_neurons+j,:]=B[j,:]*-1.#*gap#derivative of network output with respect to b1
            jacobian[(D-1)*num_neurons+num_neurons*2+j,:]=y_1[j,:]*-1.#*gap #derivative of network output with respect to w2
            jacobian[(D-1)*num_neurons+num_neurons*3,:]=1.*-1.#*gap

        hessian=np.dot(jacobian,jacobian.T)
        hessian*=1./len(y_target)
        gap*=1./len(y_target)
        gradient=np.dot(jacobian,gap.T)#*error_rmse   WE MULTIPLY BY THE GAP HERE !!

        try:
            HM1=np.linalg.inv(hessian+mu*np.eye(len(hessian[:,0])))
        except np.linalg.LinAlgError:
            print("Not invertible. Go small step in gradient direction")
            weights_update = 1.0/1e10 * gradient
        else:
            weights_update=np.dot(-HM1,gradient)

        weigths_network=np.concatenate((w1.flatten(),b1))
        b2_a=np.zeros((1,))
        b2_a[0]=b2
        weigths_network=np.concatenate((weigths_network,w2))
        weigths_network=np.concatenate((weigths_network,b2_a))
        weigths_network_updated=weigths_network+weights_update

        currenterror=np.sum(error)
        if currenterror<old_error:
            p+=1
            m=0
            if p>1: 
                mu=mu*mu_dec
                p=0
        elif currenterror==old_error or m==10:
            m=1
            mu=mu*mu_inc
        elif currenterror>old_error and m<10:
            p=0
            m+=1
            mu=mu*mu_inc
            weigths_network_updated=weigths_network
        if currenterror<target_error:
            break
        old_error=currenterror
        if test > old_error_test or test==old_error_test:
            if g>30:    #The optimization stops if the error test set increases 30 iteratitions after the other
                break 
            else:
                g+=1
        else: 
            g=0
        old_error_test=test
        for i in range(num_neurons):
            b1[i]=weigths_network_updated[(D-1)*num_neurons+num_neurons+i]
            for k in range(D):
                w1[k,i]=weigths_network_updated[k*num_neurons+i]
            b2=weigths_network_updated[-1]
            w2[i]=weigths_network_updated[(D-1)*num_neurons+num_neurons*2+i]

        epoch+=1
        if shuffle:
            unison_shuffled_copies(x_train_original,y_target_original)
        if verbose==True:
            print("epoch",epoch)#,w1.flatten())

#OUTPUT

    y_out,y_1,z=potential(x_train,w1,b1,w2,b2)
    error=np.sqrt(1./len(y_out)*np.sum(np.square(y_target-y_out))) #RMSE OF THE NORMALIZED DATA

    y_net_test,trash,trash1=potential(x_test,w1,b1,w2,b2)
    test=np.sqrt(1./len(y_test)*np.sum(np.square(y_test-y_net_test)))
    
    if verbose==True:
        print(error)

    results=[[" ", "Error", "Number of points"],["Training Set",error,x_train.shape[0]],["Test Set",test,x_test.shape[0]],["","Final","Maximum"],["Epochs",epoch,epochs]]
    print("\n","\u0332".join(str(string_network)),":\n")
    print(tabulate(results,tablefmt="fancy_grid"))
    print("Error = {1/(number_points) * sum[(y_set - y_network)^2]}^1/2")

#UNNORMALIZATION OF THE NETWORK PARAMETERS
    for i in range(num_neurons):
        w2[i]=w2[i]*(energy_max-energy_min)/(boundariese[1]-boundariese[0])
        for k in range(D):
            b1[i]+=(w1[k,i])*(boundariesx[0]-(boundariesx[1]-boundariesx[0])*(x_min[k])/(x_max[k]-x_min[k]))
            w1[k,i]=((boundariesx[1]-boundariesx[0])/(x_max[k]-x_min[k]))*w1[k,i] 
    b2=(b2-boundariese[0])*(energy_max-energy_min)/(boundariese[1]-boundariese[0])+energy_min

    return(w2,b2,w1,b1)

