#!/usr/bin/env python
import sys
import os
sys.path.append('source/')

import numpy as np
from constants import *
import subprocess
import uuid
from jinja2 import Template
import yaml

from constants import *
from basis_function import *
from potential import *
from itertools import combinations,permutations

def my_function_to_learn(xy,current_term,minimum_coordinates,temperature): 
  point_original=xy
  if len(current_term)==1:
    xy=[xy]
  data_base_folder=str(os.getcwd())+"/"+"computed_points/"
  if not os.path.exists(data_base_folder):
    os.mkdir(data_base_folder)
  filename_expansion="CO2_3D_CORR.txt"
  expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)
  threshold=expansion['Threshold(cm-1)']/wavenumber_per_ua

  comparison=0
  random_number=0
  current_order=len(current_term)
  if current_order!=1: #THERE ARE NO PARENTS FOR THE 1D cuts
    #We loop over the parent_terms
    total_output=0
    point=xy
    for order in range(1,current_order): #The loop goes up to current_order-1
      for combi in combinations(current_term,order):
        if list(combi)!=[] and len(list(combi))==1:
          parent_term=list(combi)
          if expansion[str(parent_term)]['status']!="Optimized": #Do not consider terms that were skipped
            continue 
          else:
            parent_order=len(parent_term)
            parent_point=np.zeros(parent_order)
            parent_w2=expansion[str(parent_term)]['w2'] #what are the parameters of the parent terms (of lower order) 
            parent_b2=expansion[str(parent_term)]['b2']
            parent_w1=expansion[str(parent_term)]['w1']
            parent_b1=expansion[str(parent_term)]['b1']
            parent_minimas=expansion[str(parent_term)]['minimas']
            parent_coordinates_types=expansion[str(parent_term)]['coordinates_types']
            t=0
            for i in range(parent_order):
              index=np.where(np.array(current_term)==parent_term[i])
              index=int(index[0][0])
              parent_point[i]=point[index]
              #FOR THAT PARENT POINT (ASSOCIATED TO PARENT TERM), WE COMPUTE THE POTENTIAL OUTPUT
            parent_y_output,dump,dump=potential(parent_point,parent_w1,parent_b1,parent_w2,parent_b2)
            #We simply dump the derivative here, there is no overhead for its calculation anyways.
            total_output+=parent_y_output
    np.random.seed(int.from_bytes(os.urandom(4), byteorder='little')) #Numpy doesnt play well with multiprocessing
    random_number=np.random.rand(1)[0]
    comparison=(threshold-total_output)/threshold
    if len(current_term)>0:#==2:
      comparison=comparison*2 

  #equi_geometry=[1.308,1.83,1.320,30.5,104.5,180.0] #Define first the equilibrium geometry
  geometry=np.zeros(len(minimum_coordinates[0]))
  for i in range(len(minimum_coordinates[0])): #From the number of arguments (len of the list given by adaptative), assign 
    if i in current_term: #->geometry coordinates to the term element (current_term). 
#      print(np.where(np.array(current_term)==i)[0][0])
      geometry[i]=xy[np.where(np.array(current_term)==i)[0][0]]
    else:
      geometry[i]=minimum_coordinates[0][i]

  #There is a MAPPING of the coordinates to do! 
  #Reminder: [RCO,RCH,RCF,AHOC,AFOC,DFHOC]
  #The valence coordinates: [RCO{BOHR},RCH{BOHR},RCF{BOHR},cos(AHOC{RAD}),cos(AHOC{RAD}},DFHOC{RAD}]
  #The procedure will provide the lengths in Bohr, molpro will use them in angstrom
  geometry_mapped=np.zeros(len(geometry))
  geometry_mapped[0]=geometry[0]/angstrom_to_bohr
  geometry_mapped[1]=geometry[1]/angstrom_to_bohr
  geometry_mapped[2]=geometry[2]/radian_per_degree


  if comparison>random_number or len(current_term)==1 :
    #Use this list that define the geometry in the coordinates (and units) of the z-matrix in a template of the molpro calculation. 
    template_molpro_co2=''' ***,co2
  symmetry,z
  R1=1.16
  R2=1.16
  AOCO=179.
 
  geometry={angstrom
            C;
            O1,C,R1;
            O2,C,R2,O1,AOCO}
  basis=AVQZ
  !hf
  {multi;wf,22,1,0}!;state,3}

  R1={{r_1}}
  R2={{r_2}}
  AOCO={{a_oco}}
  {multi;wf,22,1,0}!;state,3}
  multie=energy(1)
!  multif=energy(2)
!  multig=energy(3)
'''

    tm=Template(template_molpro_co2)

    input_file=tm.render(
    r_1=geometry_mapped[0],
    r_2=geometry_mapped[1],
    a_oco=geometry_mapped[2],
    )

    #print(input_file)
    unique_filename = str(uuid.uuid4())
    directory_path=str(os.getcwd())+"/"+"co2"+unique_filename+"/"
    if not os.path.exists(directory_path):
      os.mkdir(directory_path)

    input_file_path=directory_path+"co2"+".inp"
    with open(input_file_path, 'w') as f:
       print(input_file, file=f)
    f.close()
    #Once the input file is created, run the program
    exit_status=os.system('''cd '''+str(directory_path)+'''&& /home/nvaeck/Softwares/Molpro2022.2/bin/molpro.exe -v -t 3 '''+'''co2.inp'''+'''>>/dev/null''')
    if exit_status!=0:
      print("The molpro command failed")
      energy=10000000.0
      energy1=10000000.0  
    else:
      #Once this is over, get what you are interested in from the output
      proc = subprocess.Popen('grep -r "SETTING MULTIE" '+str(directory_path)+'''co2.out | head -1 | cut -d "=" -f 2 | tr -s ' '| cut -d ' ' -f 2 | head -n 1''', stdout=subprocess.PIPE, shell=True)
#      proc1 = subprocess.Popen('grep -r "SETTING MULTIF" '+str(directory_path)+'''co2.out | head -1 | cut -d "=" -f 2 | tr -s ' '| cut -d ' ' -f 2 | head -n 1''', stdout=subprocess.PIPE, shell=True)
#      proc2 = subprocess.Popen('grep -r "SETTING MULTIG" '+str(directory_path)+'''co2.out | head -1 | cut -d "=" -f 2 | tr -s ' '| cut -d ' ' -f 2 | head -n 1''', stdout=subprocess.PIPE, shell=True)
      #proc = subprocess.Popen("awk '/SETTING ESCF/{print $4;exit}' "+str(directory_path)+'''hono.out''', stdout=subprocess.PIPE, shell=True)
      energy=proc.communicate()[0]
#      energyf=proc1.communicate()[0]
#      energyg=proc2.communicate()[0]
      energy1=energy
      #Keep it somewhere
      path_file_db=data_base_folder+"co2_tab.txt"
      newline=str(current_term).replace(' ','')+' '
      for i in range(len(geometry_mapped)):
        newline+="{:.8f}".format(geometry_mapped[i])+' '
      newline+="{:.8f}".format(float(energy))+' '
#      newline+="{:.8f}".format(float(energyf))+' '
#      newline+="{:.8f}".format(float(energyg))+' '
      print(newline,file=open(path_file_db,"a"))

      path_file_db_mapped=data_base_folder+"co2_tab_mapped.txt"
      newline=str(current_term).replace(' ','')+' '
      for i in range(len(geometry)):
        newline+="{:.8f}".format(geometry[i])+' '
      newline+="{:.8f}".format(float(energy)-minimum_coordinates[1][0])+' '
#      newline+="{:.8f}".format(float(energyf)-minimum_coordinates[1][0])+' '
#      newline+="{:.8f}".format(float(energyg)-minimum_coordinates[1][0])+' '
      print(newline,file=open(path_file_db_mapped,"a"))
  
    os.system("rm -rf "+str(directory_path)) 
  else:
    #print("point filtered out")
    energy=total_output+minimum_coordinates[1][0]
    energy1=41000./219474.+minimum_coordinates[1][0]
  
  #VALUE OF THE CURRENT FIT
  #For each point, we compute the output of the expansion
  output_expansion=0.0
  #For each term:
  for current_term in expansion['full_list_of_terms']:
    if expansion[str(current_term)]['status']=="Optimized" or expansion[str(current_term)]['status']=="Ongoing": #Do not consider terms that were skipped
      str_term=str(current_term)
      current_order=len(current_term)
      current_w2=expansion[str_term]['w2']
      current_b2=expansion[str_term]['b2']
      current_w1=expansion[str_term]['w1']
      current_b1=expansion[str_term]['b1']
      current_minimas=expansion[str(current_term)]['minimas']
      current_coordinates_types=expansion[str(current_term)]['coordinates_types']
      point_original=np.zeros(current_order)
      for i in range(current_order): #From the number of arguments (len of the list given by adaptative), assign 
        if i in current_term: #->geometry coordinates to the term element (current_term). 
          point_original[i]=xy[np.where(np.array(current_term)==i)[0][0]]
      current_y_output=potential(point_original,current_w1,current_b1,current_w2,current_b2)[0]
      output_expansion+=current_y_output
  error=(float(energy)-minimum_coordinates[1][0])-output_expansion
  return {'energ':float(energy)-minimum_coordinates[1][0],'energ_filter':float(energy1)-minimum_coordinates[1][0],'error':error} #ATTENTION RETURN THE OPPOSITE OF THE VALUE IF YOU WANT THE SAMPLING TO BE BETTER AROUND THE MINIMUM RATHER THAN THE MAXIMUM OF THE ENERGY
                                          #WE PROBABLY DON'T WANT THAT FOR THE CORRELATION
