import yaml
from useful_functions import *
# GENERATE THE OPERATOR FILE FOR MCTDH --> EVERYTHING SHOULD COME FROM THE EXPANSION, THAT WAY YOU CAN GENERATE IT FROM THE EXPANSION SAVED IN THE FILE
def MCTDH_operator(filename_expansion,operator_file_name,name_of_the_expansion):
  expansion = yaml.load(open(filename_expansion,'r'))

  op_1='''OP_DEFINE-SECTION
title
HFCO in valence coordinates. r1=r(C-H), r2=r(C-F), r3=r(C-O), beta1=(O-C-H), beta2=(O-C-F)
phi = angle (O-C-F) and (O-C-H).
end-title
end-op_define-section

PARAMETER-SECTION
mh  = 1.0, H-mass
mc  = 12.00,AMU
mo  = 15.9949146221,AMU
mf  = 18.99840320,AMU
M11 = 1.0/mh+1.0/mc
M22 = 1.0/mf+1.0/mc
M33 = 1.0/mo+1.0/mc
Mu  = 1.0/mc            # Mu = Mij; i neq j
'''

  op_2='''end-parameter-section
LABELS-SECTION'''
  print(op_1,file=open(operator_file_name, "a"))

  filename=operator_file_name
  print("PARAMETER-SECTION",file=open(filename, "a"))

  t=0
  for current_term in expansion['full_list_of_terms']:
    #print(current_term)
    if expansion[str(current_term)]['status']=="Skip": #Do not consider terms that were skipped
      print("We skipped term ",str(current_term))
      break
    v_shift=expansion[str(current_term)]['b2']
    Cq=expansion[str(current_term)]['w2']
    weights=expansion[str(current_term)]['w1']
    bq=expansion[str(current_term)]['b1']
    D=len(current_term)
    print("c",'T',t,'=',v_shift,file=open(filename, "a"),sep='')

    for i in range(len(Cq)):
      print("r",i,'T',t,"=",Cq[i]*np.exp(bq[i]),file=open(filename, "a"),sep='') #Multiplicative coefficients, Cq*exp(b)
      for j in range(D):
        print("w",i,"u",j,'T',t,"=",weights[j,i],file=open(filename, "a"),sep='') #Weights arguments of the exponential w as 
    t+=1

  print("""end-parameter-section
LABELS-SECTION""",file=open(filename, "a"))


  t=0
  for current_term in expansion['full_list_of_terms']:
    #print(current_term)
    if expansion[str(current_term)]['status']=="Skip": #Do not consider terms that were skipped
      print("We skipped term ",str(current_term))
      break
    v_shift=expansion[str(current_term)]['b2']
    Cq=expansion[str(current_term)]['w2']
    weights=expansion[str(current_term)]['w1']
    bq=expansion[str(current_term)]['b1']
    D=len(current_term)



    for i in range(len(Cq)):
      for j in range(D):
        print("q",i,"u",current_term[j],'T',t,"=exp[w",i,"u",j,'T',t,",0,0]",file=open(filename, "a"),sep='')
    t+=1
  print("end-labels-section",file=open(filename, "a")) 
  print("HAMILTONIAN-SECTION",file=open(filename, "a"))
  print("-----------------------------------------------",file=open(filename, "a"))
  print("modes       ",end='',file=open(filename, "a"))
  for j in range(D):
    print("|  x_",j,'     ',end='',file=open(filename, "a"),sep='')
  print(" ",file=open(filename, "a"))
  print("-----------------------------------------------",file=open(filename, "a"))
 
  t=0
  for current_term in expansion['full_list_of_terms']:
    #print(current_term)
    if expansion[str(current_term)]['status']=="Skip": #Do not consider terms that were skipped
      print("We skipped term ",str(current_term))
      break
    v_shift=expansion[str(current_term)]['b2']
    Cq=expansion[str(current_term)]['w2']
    weights=expansion[str(current_term)]['w1']
    bq=expansion[str(current_term)]['b1']
    D=len(current_term)

    print("c",'T',t,"       ",end='',file=open(filename, "a"),sep='')
    for j in range(D):
      print("|    1 ",end='',file=open(filename, "a"))
    print(" ",file=open(filename, "a"))
    for i in range(len(Cq)):
      print("r",i,'T',t,end=' ',file=open(filename, "a"),sep='')
      for j in range(expansion['number_of_dimensions']):
        if j in current_term:
          print("|   q",i,"u",j,'T',t,end='',file=open(filename, "a"),sep='')
        else:
          print("|    1 ",end='',file=open(filename, "a"))
      print(" ",file=open(filename, "a"))
    t+=1
  print("-----------------------------------------------",file=open(filename, "a"))
  print("end-hamiltonian-section",file=open(filename, "a"))


  op_6='''end-operator
'''
  print(op_6,file=open(operator_file_name, "a"))

