import numpy as np
#REMOVE DUPLICATES FROM A LIST
def remove_duplicates(x):
  return list(dict.fromkeys(x))
def scaling_datasets2(D,x_train,y_target,x_test,y_test,boundariesx,boundariese):
    x_min=np.zeros(D)
    x_max=np.zeros(D)
    x_train_scaled=np.zeros((x_train.shape[0],x_train.shape[1]))
    x_test_scaled=np.zeros((x_test.shape[0],x_test.shape[1]))
    y_test_scaled=np.zeros((y_test.shape[0]))
    for k in range(D):
        x_min[k]=np.amin(x_train[:,k])
        x_max[k]=np.amax(x_train[:,k])
    energy_min=np.amin(y_target)
    energy_max=np.amax(y_target)

    for k in range(D):
        x_train_scaled[:,k]=normalize(x_min[k],x_max[k],x_train[:,k],boundariesx)
        x_test_scaled[:,k]=normalize(x_min[k],x_max[k],x_test[:,k],boundariesx)
    y_target_scaled=normalize(energy_min,energy_max,y_target,boundariese)
    y_test_scaled=normalize(energy_min,energy_max,y_test,boundariese)
    return (x_train_scaled,y_target_scaled,x_test_scaled,y_test_scaled,energy_max,energy_min,x_min,x_max)

def normalize(z_min,z_max,a_list,bounds):
    #z_* are floats, a_list is a list, and boundaries is a list of two values
    b=np.zeros(len(a_list))
    for j in range(len(a_list)):
         b[j]=bounds[0]+((bounds[1]-bounds[0])*(a_list[j]-z_min))/(z_max-z_min)
    return b

def unnormalize(z_min,z_max,a_list,bounds):
    b=np.zeros(len(a_list))
    for j in range(len(a_list)):
        b[j]=(a_list[j]-bounds[0])*(z_max-z_min)/(bounds[1]-bounds[0])+z_min
    return b

def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]

