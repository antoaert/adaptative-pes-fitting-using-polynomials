import yaml
import numpy as np
from potential import *
filename_expansion="HFCO_3D_HF.txt"
expansion = yaml.load(open(filename_expansion,'r'))
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
#GENERATE THE 2D TERMS, HOW DO THEY LOOK LIKE ?
batchsize=500 #number of points to plot
COORDINATES=[0,2] #int that defines the coordinate that you want to plot
str_term='['+str(COORDINATES[0])+', '+str(COORDINATES[1])+']'
print(expansion['boundaries_coordinates'])
xy_min = [expansion['boundaries_coordinates'][COORDINATES[0]][0],expansion['boundaries_coordinates'][COORDINATES[1]][0]]
xy_max = [expansion['boundaries_coordinates'][COORDINATES[0]][1],expansion['boundaries_coordinates'][COORDINATES[1]][1]]

current_w2=expansion[str_term]['w2']
current_b2=expansion[str_term]['b2']
current_w1=expansion[str_term]['w1']
current_b1=expansion[str_term]['b1']

current_minimas=expansion[str_term]['minimas']
current_coordinates_types=expansion[str_term]['coordinates_types']
y_out=np.zeros(batchsize)
points_on_grid=np.random.uniform(low=xy_min, high=xy_max, size=(batchsize,2))
output_expansion=np.zeros(batchsize)
t=0
for point in points_on_grid:
  y_out[t]=potential(point,current_w1,current_b1,current_w2,current_b2)[0]
  t+=1
x=points_on_grid[:,0]
y=points_on_grid[:,1]
z=y_out

fig = plt.figure()
ax = Axes3D(fig)
surf = ax.plot_trisurf(x, y, z, cmap=cm.jet, linewidth=0.1)
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()
