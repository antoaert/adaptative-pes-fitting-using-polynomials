import yaml
from itertools import combinations,permutations
from basis_function import *
from constants import *

def initialize_expansion(filename_expansion,IsThisARestart,number_of_dimensions,max_order,name_of_coordinates,type_of_coordinates,boundaries_coordinates,local_bounds,minimum_coordinates,skip,skip_threshold,rmse_target_fit,rmse_target_newpoints,EnergyThreshold,energy_threshold,WasTheSignChanged,DoISaveTheExpansion,Verbose_comment):

  #We build a dictionary that defines the expansion.
  #The expansion is defined by a number of terms, each of which acts on one or 
  #multiple coordinates. Each of the term is build from a sum of products of 
  #functional, the type of functions differ from the type of coordinate that it
  #needs to fit. We fit the data with increasing order of the terms (1D first ...)
  #and the sum of product shall follow an increasing order of the functional 
  #(q^2 + q^3 + q^4 ...) in 1D for example. 
  WeHaveToReBuild=False
  if IsThisARestart:
    #HOW TO READ THE DICTIONARY AGAIN?
    expansion_read = yaml.load(open(filename_expansion, 'r'))
    #print(expansion_read)
    expansion=expansion_read
    #CHECK WHAT HAS CHANGED SINCE THE LAST TIME
    if expansion['max_order']!=max_order:
      expansion['max_order']=max_order
    if expansion['type_of_coordinates']!=type_of_coordinates:
      expansion['type_of_coordinates']=type_of_coordinates
      WeHaveToReBuild=True
    if expansion['minimum_coordinates']!=minimum_coordinates:
      print("You cannot change the minimum coordinates without having to reoptimize all the terms. ")
      expansion['minimum_coordinates']=minimum_coordinates
      WeHaveToReBuild=True
    if expansion['boundaries_coordinates']!=boundaries_coordinates:
      expansion['boundaries_coordinates']=boundaries_coordinates
      WeHaveToReBuild=True
    if expansion['rmse_target_fit']!=rmse_target_fit:
      expansion['rmse_target_fit']=rmse_target_fit
    if expansion['rmse_target_newpoints']!=rmse_target_newpoints:
      expansion['rmse_target_newpoints']=rmse_target_newpoints
    if EnergyThreshold and expansion['Threshold(cm-1)']!=energy_threshold*wavenumber_per_ua:
      expansion['Threshold(cm-1)']=energy_threshold*wavenumber_per_ua
    if expansion['WasTheSignChanged']!=WasTheSignChanged:
      expansion['WasTheSignChanged']=WasTheSignChanged
    if expansion['VerboseComment']!=Verbose_comment:
      expansion['VerboseComment']=Verbose_comment
    if expansion['SkipThreshold']!=skip_threshold:
      expansion['SkipThreshold']=skip_threshold
    


  if not(IsThisARestart) or WeHaveToReBuild:
    expansion={}
    terms_list=list()
    functional_sequencelist=list()
    for order in range(0,number_of_dimensions+1):
      for c in combinations(range(number_of_dimensions),order): 
        if list(c) != []:
          terms_list.append(list(c))
          coordinates_types=list()
          minimas=np.zeros(order)
          term=list(c)
          for z in range(order):
            #What are the types of coordinates for this term
            coordinates_types.append(str(type_of_coordinates[term[z]]))
            minimas[z]=minimum_coordinates[0][term[z]]
            if list(c) in list(skip):
              status="Skip"
            else:
              status="NeverSeen"
            expansion[str(list(c))]={'status':status,'w2':np.zeros((1)),'b2':np.zeros((1)),'w1':np.zeros((len(list(c)),1)),'b1':np.zeros((1)),'coordinates_types':coordinates_types,'minimas':minimas.tolist(),'fit_points':0,'valid_points':0,'number_of_points':0,'rmse_newpoints':1.0,'coupling_significance':1.}
    expansion['number_of_dimensions']=number_of_dimensions
    expansion['full_list_of_terms']=terms_list
    expansion['max_order']=max_order
    expansion['name_of_coordinates']=name_of_coordinates
    expansion['type_of_coordinates']=type_of_coordinates
    expansion['minimum_coordinates']=minimum_coordinates
    expansion['boundaries_coordinates']=boundaries_coordinates
    expansion['local_bounds']=local_bounds
    expansion['rmse_target_fit']=rmse_target_fit
    expansion['rmse_target_newpoints']=rmse_target_newpoints
    if EnergyThreshold:
      expansion['Threshold(cm-1)']=energy_threshold*wavenumber_per_ua
      expansion['EnergyThreshold']=EnergyThreshold
    expansion['WasTheSignChanged']=WasTheSignChanged
    expansion['VerboseComment']=Verbose_comment
    expansion['SkipThreshold']=skip_threshold

#  print(expansion)

  #KEEP THE EXPANSION IN A FILE:
  if DoISaveTheExpansion:
    file_expansion = open(filename_expansion, "w")
    yaml.dump(expansion, file_expansion)
    file_expansion.close()
