import numpy as np
import matplotlib.pyplot as plt # for plotting
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from numpy import exp,arange
from pylab import meshgrid,cm,imshow,contour,clabel,colorbar,axis,title,show
import tempfile
import shutil
from tabulate import tabulate

################ USER PARAM ##################
num_dimensions=6
#num_neurons_rough=15
#num_neurons_diff=65
num_neurons_rough_SCF=75        #Number of neurons of the first hidden layer
num_neurons_diff_SCF=25
num_neurons_diff2_SCF=25
num_neurons_rough_corr=5
num_neurons_diff_corr=30

scaling=True            #The scaling does min-max scaling to the given bounds (see lines below)
boundariesx=[-1.5,1.5]    #Boundaries of the scaling for the geometry parameters
boundariese=[0,5.]    #Boundaries of the scaling for the output (energy) of the network (target)
                        # Recommended : [-1.,1.]
epochs_rough_SCF=300
epochs_diff_SCF=200
energy_limit=10000 #in cm-1

NGW=True #We use the NGuyen Widrow initialization, if False, will have to provide the weights and biases
target_error=0.00000005
################ USER PARAM #END##############
D=num_dimensions
angstrom_to_bohr=1./0.529177210903
radian_per_degree=0.0174533

################ DATASETS ####################


#Read the dataset as in my database
database_path="/Users/antoineaerts/Icloud/Thèse/Antoine_Articles/AlbertaPESFitting/Code/Database/"
Folder_names=["HF_3D_moretest/"]

#filename=str(database_path)+str(Folder_names[0])+"hfcoA"
#train_set = np.genfromtxt(filename)

filename=str(database_path)+str(Folder_names[0])+"TrainingSet/hfcoB_unique"
train_set_HF = np.genfromtxt(filename)

filename=str(database_path)+str(Folder_names[0])+"TestSet/hfcoB_unique"
test_set = np.genfromtxt(filename)

filename=str(database_path)+str(Folder_names[0])+"ValidSet/hfcoB_unique"
valid_set = np.genfromtxt(filename)

#Need to rearrange the coordinates since the kinetic operator uses:
#   RCH |   RCF |   RCO |   cos(theta1) |   cos(theta2) |   Phi
# All lengths must be in bohr, angle in radians
y_target_SCF=np.zeros((len(train_set_HF)))
y_target_SCF=train_set_HF[:,7]#Attention takes the SCF energy for the minimum in energy and the criterion for the limit  
min_energy=min(train_set_HF[:,7])
i=len(train_set_HF)-1
np.sort(train_set_HF[:,7])
#print("done sorting")
train_set_HF=train_set_HF[train_set_HF[:,7]<min_energy+energy_limit/219474.]
test_set=test_set[test_set[:,7]<min_energy+energy_limit/219474.]
valid_set=valid_set[valid_set[:,7]<min_energy+energy_limit/219474.]
#while i>0:
#    if train_set_HF[i,7]>min_energy+10000./219474.:  #Remove the energy points that are above 40000 wavenumbers
#        train_set_HF=np.delete(train_set_HF,(i),axis=0) 
#    i-=1 
print("training set size:"+str(len(train_set_HF[:,7])))

#i=len(test_set)-1
#while i>0:
#    if test_set[i,7]>min_energy+10000./219474.:  #Remove the energy points that are above 40000 wavenumbers
#        test_set=np.delete(test_set,(i),axis=0) 
#    i-=1 
print("test set size:"+str(len(test_set[:,7])))

#i=len(valid_set)-1
#while i>0:
#    if valid_set[i,7]>min_energy+10000./219474.:  #Remove the energy points that are above 40000 wavenumbers
#        valid_set=np.delete(valid_set,(i),axis=0) 
#    i-=1 
print("valid set size:"+str(len(valid_set[:,7])))



y_target_SCF=np.zeros((len(train_set_HF)))
y_target_SCF=train_set_HF[:,7]-min_energy

#for i in range(len(y_target_SCF)):
#    print(y_target_SCF[i])
#print(min(y_target_SCF))
#exit()

y_test_SCF=np.zeros((len(test_set)))
y_test_SCF=test_set[:,7]-min_energy
y_valid_SCF=np.zeros((len(valid_set)))
y_valid_SCF=valid_set[:,7]-min_energy

x_train=np.zeros((len(train_set_HF),D))
x_test=np.zeros((len(test_set),D))
x_valid=np.zeros((len(valid_set),D))

#numpy cos takes input in radians!!

x_train[:,0]=train_set_HF[:,4] #RCH
x_train[:,0]=x_train[:,0]*angstrom_to_bohr #RCH in Bohr
x_train[:,1]=train_set_HF[:,2] #RCF
x_train[:,1]=x_train[:,1]*angstrom_to_bohr
x_train[:,2]=train_set_HF[:,1] #RCO
x_train[:,2]=x_train[:,2]*angstrom_to_bohr
x_train[:,3]=train_set_HF[:,5] #Theta HCO
x_train[:,3]=np.cos(x_train[:,3]*radian_per_degree)
x_train[:,4]=train_set_HF[:,3] #Theta OCF
x_train[:,4]=np.cos(x_train[:,4]*radian_per_degree)
x_train[:,5]=train_set_HF[:,6] #Dihedral (Phi)
x_train[:,5]=x_train[:,5]*radian_per_degree


x_test[:,0]=test_set[:,4] #RCH
x_test[:,0]=x_test[:,0]*angstrom_to_bohr #RCH in Bohr
x_test[:,1]=test_set[:,2] #RCF
x_test[:,1]=x_test[:,1]*angstrom_to_bohr
x_test[:,2]=test_set[:,1] #RCO
x_test[:,2]=x_test[:,2]*angstrom_to_bohr
x_test[:,3]=test_set[:,5] #Theta HCO
x_test[:,3]=np.cos(x_test[:,3]*radian_per_degree)
x_test[:,4]=test_set[:,3] #Theta OCF
x_test[:,4]=np.cos(x_test[:,4]*radian_per_degree)
x_test[:,5]=test_set[:,6] #Dihedral (Phi)
x_test[:,5]=x_test[:,5]*radian_per_degree

x_valid[:,0]=valid_set[:,4] #RCH
x_valid[:,0]=x_valid[:,0]*angstrom_to_bohr #RCH in Bohr
x_valid[:,1]=valid_set[:,2] #RCF
x_valid[:,1]=x_valid[:,1]*angstrom_to_bohr
x_valid[:,2]=valid_set[:,1] #RCO
x_valid[:,2]=x_valid[:,2]*angstrom_to_bohr
x_valid[:,3]=valid_set[:,5] #Theta HCO
x_valid[:,3]=np.cos(x_valid[:,3]*radian_per_degree)
x_valid[:,4]=valid_set[:,3] #Theta OCF
x_valid[:,4]=np.cos(x_valid[:,4]*radian_per_degree)
x_valid[:,5]=valid_set[:,6] #Dihedral (Phi)
x_valid[:,5]=x_valid[:,5]*radian_per_degree

#print(min(x_train[:,5]))
#print(max(x_train[:,5]))
#exit()

'''
for i in range(6):
    print("xtrain_min and max")
    print(min(x_train[:,i]), max(x_train[:,i]))
    print("xtest_min and max")
    print(min(x_test[:,i]), max(x_test[:,i]))
    print("xvalid_min and max")
    print(min(x_valid[:,i]), max(x_valid[:,i]))
    print(min(y_target_SCF),max(y_target_SCF))
    print(min(y_test_SCF),max(y_test_SCF))
    print(min(y_valid_SCF),max(y_valid_SCF))
exit()
'''


'''
y_target_corr=np.zeros((len(train_set)))
y_target_corr=train_set[:,8]-min_energy
y_test_corr=np.zeros((len(test_set)))
y_test_corr=test_set[:,8]-min_energy
y_valid_corr=np.zeros((len(valid_set)))
y_valid_corr=valid_set[:,8]-min_energy


for i in range(len(train_set)):
    y_target_corr[i]=y_target_corr[i]-y_target_SCF[i]
for i in range(len(test_set)):
    y_test_corr[i]=y_test_corr[i]-y_test_SCF[i]
for i in range(len(valid_set)):
    y_valid_corr[i]=y_valid_corr[i]-y_valid_SCF[i]
'''

def my_target(x,y):
#    return(x**2.+y**2.)
    return((1.-x)**2.+50.*(y-x**2.)**2.) #ROSENBROCK

#x_train has size (batchsize,num_dimensions)
#For PES fitting, y_train has size (batchsize, 1) 
#Same for the others but batchsize is reduced.

#BATCHSIZE=10000
#BATCHSIZE_T=500
#x_train=np.random.uniform(low=-1.5,high=1.5,size=[BATCHSIZE,2]) #BATCHSIZE=750

#y_target=my_target(x_train[:,0],x_train[:,1])
#x_test=np.random.uniform(low=-1.43,high=1.5,size=[BATCHSIZE_T,2])
#y_test=my_target(x_test[:,0],x_test[:,1])
#x_valid=np.random.uniform(low=-1.63,high=1.6,size=[45,2])
#y_valid=my_target(x_valid[:,0],x_valid[:,1])

################ DATASETS #END################

def main():
    global x_train,y_target,x_test,y_test,x_valid,y_valid,epochs,num_neurons,num_dimensions,scaling,boundariesx,boundariese,NGW,target_error
    if scaling==True:
        (x_train_scaled,y_target_scaled,x_valid_scaled,y_valid_scaled,x_test_scaled,y_test_scaled,energy_max,energy_min,x_min,x_max)=scaling_datasets(D,x_train,y_target_SCF,x_valid,y_valid_SCF,x_test,y_test_SCF,boundariesx,boundariese)
        #They all need to be scaled with the same parameters
#    x_train_sort1=np.sort(x_train,axis=0)
#    x_train_sort2=np.sort(x_train,axis=1)


    w1_rough_SCF=np.zeros((num_dimensions,num_neurons_rough_SCF))
    b1_rough_SCF=np.zeros((num_neurons_rough_SCF))
    w2_rough_SCF=np.zeros((num_neurons_rough_SCF))
    b2_rough_SCF=np.zeros((1))
    w1_diff_SCF=np.zeros((num_dimensions,num_neurons_diff_SCF))
    b1_diff_SCF=np.zeros((num_neurons_diff_SCF))
    w2_diff_SCF=np.zeros((num_neurons_diff_SCF))
    b2_diff_SCF=np.zeros((1))
 

    #Train the network with initialized values
    w2_rough_SCF,b2_rough_SCF,w1_rough_SCF,b1_rough_SCF=training_network(x_train_scaled,y_target_scaled,x_test_scaled,y_test_scaled,x_valid_scaled,y_valid_scaled,num_neurons_rough_SCF,num_dimensions,epochs_rough_SCF,NGW,energy_max,energy_min,x_min,x_max,boundariesx,boundariese,name="Rough surface",verbose=True,minibatch=1)
    

    y_out_train,dump,dump1=apply_network(x_train,w1_rough_SCF,b1_rough_SCF,w2_rough_SCF,b2_rough_SCF)
    y_out_test,dump,dump1=apply_network(x_test,w1_rough_SCF,b1_rough_SCF,w2_rough_SCF,b2_rough_SCF)
    y_out_valid,dump,dump1=apply_network(x_valid,w1_rough_SCF,b1_rough_SCF,w2_rough_SCF,b2_rough_SCF)

    #RMSE OF NETWORK OUTPUT
#    error=0.5*(np.square(y_target_SCF-y_out_train))        #Error of the training set fit
#    for i in range(len(error)): 
#        print(y_target_SCF[i],y_out_train[i],error[i])
    error_rmse=np.sqrt((1./len(x_train))*np.sum(np.square(y_target_SCF-y_out_train)))

    print("TRAIN SCF network RMSE in cm-1",error_rmse*219474.)
    for i in range(len(x_train[:,5])):
        print(x_train[i,0],x_train[i,1],x_train[i,2],x_train[i,3],x_train[i,4],x_train[i,5],y_target_SCF[i],y_target_SCF[i]-y_out_train[i],file=open("error.txt", "a"))
    error_test_rmse=np.sqrt((1./len(x_test))*np.sum(np.square(y_test_SCF-y_out_test)))
    print("TEST SCF network RMSE in cm-1",error_test_rmse*219474.)

    error_valid_rmse=np.sqrt((1./len(x_valid))*np.sum(np.square(y_valid_SCF-y_out_valid)))
    print("VALID SCF network RMSE in cm-1",error_valid_rmse*219474.)






    #Compute the difference that will be fitted by the other NN
    y_target_diff=np.subtract(y_target_SCF,y_out_train)
    y_test_diff=np.subtract(y_test_SCF,y_out_test)
    y_valid_diff=np.subtract(y_valid_SCF,y_out_valid) 
    if scaling==True:
        (x_train_scaled_diff,y_target_scaled_diff,x_valid_scaled_diff,y_valid_scaled_diff,x_test_scaled_diff,y_test_scaled_diff,energy_max,energy_min,x_min,x_max)=scaling_datasets(D,x_train,y_target_diff,x_valid,y_valid_diff,x_test,y_test_diff,boundariesx,boundariese)
    
    w2_diff_SCF,b2_diff_SCF,w1_diff_SCF,b1_diff_SCF=training_network(x_train_scaled_diff,y_target_scaled_diff,x_test_scaled_diff,y_test_scaled_diff,x_valid_scaled_diff,y_valid_scaled_diff,num_neurons_diff_SCF,num_dimensions,epochs_diff_SCF,NGW,energy_max,energy_min,x_min,x_max,boundariesx,boundariese,name="difference",verbose=True)


#We trained two different NN, the total is the sum of the two networks
    w2=np.append(w2_rough_SCF,w2_diff_SCF)
    w1=np.append(w1_rough_SCF,w1_diff_SCF,axis=1)
    b2=b2_rough_SCF+b2_diff_SCF
    b1=np.append(b1_rough_SCF,b1_diff_SCF)


    y_out_train,dump,dump1=apply_network(x_train,w1,b1,w2,b2)
    y_out_test,dump,dump1=apply_network(x_test,w1,b1,w2,b2)
    y_out_valid,dump,dump1=apply_network(x_valid,w1,b1,w2,b2)

    #RMSE OF NETWORK OUTPUT
    error=0.5*(np.square(y_target_SCF-y_out_train))        #Error of the training set fit
#    for i in range(len(error)): 
#        print(y_target_SCF[i],y_out_train[i],error[i])
    error_rmse=np.sqrt((1./len(x_train))*np.sum(np.square(y_target_SCF-y_out_train)))

    print("TRAIN SCF network RMSE in cm-1",error_rmse*219474.)

#    w2=w2_rough_SCF
#    w1=w1_rough_SCF
#    b2=b2_rough_SCF
#    b1=b1_rough_SCF
#    print(b1)
#    print(w1)
#    print(w2)
#    print(b2) 

    mctdh_oper(num_dimensions,w2,b2,b1,w1,"network.op")
    
    ################### SOME SORT OF PLOTTING ########################


    title=str(1)+" neurons"
# plot it!
    plt.title(title)
#plt.xlim(.60,2.8)
#plt.ylim(-3.0,2.0)






    x = arange(-2.,2.,0.05)
    y = arange(-2.,2.,0.05)
    X,Y = meshgrid(x, y) # grid of point
#print(np.column_stack(([X[0], Y[0]])))
#print(X)
#print(Y)
#print([X,Y])
    Z=np.zeros((80,80))
    for i in range(len(X[:])):
        x_in=np.column_stack([X[i], Y[i]])
        Z[i],trash,trash1 = apply_network(x_in,w1,b1,w2,b2) # evaluation of the function on the grid
    fig = plt.figure(figsize=plt.figaspect(0.5))
#ax = fig.gca(projection='3d')
    ax = fig.add_subplot(1, 2, 1, projection='3d')



    surf = ax.plot_surface(X,Y,Z, rstride=1, cstride=1, 
                      cmap=cm.RdBu,linewidth=0, antialiased=False)
    Z2=np.zeros((80,80))
    for i in range(len(X[:])):
        Z2[i]=my_target(X[i],Y[i])

    ax = fig.add_subplot(1, 2, 2, projection='3d')

    surf2=ax.plot_surface(X,Y,Z2,rstride=1, cstride=1,
                      cmap=cm.RdBu,linewidth=0, antialiased=False)
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()


def scaling_datasets(D,x_train,y_target,x_valid,y_valid,x_test,y_test,boundariesx,boundariese):
    x_min=np.zeros(D)
    x_max=np.zeros(D)
    x_train_scaled=np.zeros((x_train.shape[0],x_train.shape[1]))
    x_test_scaled=np.zeros((x_test.shape[0],x_test.shape[1]))
    x_valid_scaled=np.zeros((x_valid.shape[0],x_valid.shape[1]))
    y_target_scaled=np.zeros((y_target.shape[0]))
    y_test_scaled=np.zeros((y_test.shape[0]))
    y_valid_scaled=np.zeros((y_valid.shape[0]))
    for k in range(num_dimensions):
        x_min[k]=np.amin(x_train[:,k])
        x_max[k]=np.amax(x_train[:,k])
    energy_min=np.amin(y_target)
    energy_max=np.amax(y_target)

    for k in range(D):
        x_train_scaled[:,k]=normalize(x_min[k],x_max[k],x_train[:,k],boundariesx)
        x_test_scaled[:,k]=normalize(x_min[k],x_max[k],x_test[:,k],boundariesx)
        x_valid_scaled[:,k]=normalize(x_min[k],x_max[k],x_valid[:,k],boundariesx)
    y_target_scaled=normalize(energy_min,energy_max,y_target,boundariese)
    y_test_scaled=normalize(energy_min,energy_max,y_test,boundariese)
    y_valid_scaled=normalize(energy_min,energy_max,y_valid,boundariese)
    return (x_train_scaled,y_target_scaled,x_valid_scaled,y_valid_scaled,x_test_scaled,y_test_scaled,energy_max,energy_min,x_min,x_max)

def normalize(z_min,z_max,a_list,bounds):
    #z_* are floats, a_list is a list, and boundaries is a list of two values
    b=np.zeros(len(a_list))
    for j in range(len(a_list)):
         b[j]=bounds[0]+((bounds[1]-bounds[0])*(a_list[j]-z_min))/(z_max-z_min)
    return b

def unnormalize(z_min,z_max,a_list,bounds):
    b=np.zeros(len(a_list))
    for j in range(len(a_list)):
        b[j]=(a_list[j]-bounds[0])*(z_max-z_min)/(bounds[1]-bounds[0])+z_min
    return b

def apply_exp_layer(x_in,w,b):
    z=np.dot(x_in,w)+b
    return z,np.exp(z)    #net,output,slope

def apply_linear_layer(x_in,w,b):
    z=np.dot(x_in,w)+b
    return z


def apply_network(x_in,w1,b1,w2,b2):
    z,y_1=apply_exp_layer(x_in,w1,b1)
    y_out=apply_linear_layer(y_1,w2,b2)
    return (y_out,y_1,z)



def training_network(x_train,y_target,x_test,y_test,x_valid,y_valid,num_neurons,num_dimensions,epochs,NGW,energy_max,energy_min,x_min,x_max,boundariesx,boundariese,**kwargs):
    verbose=kwargs.get('verbose', False)
    string_network=kwargs.get('name'," ")
    batch_number=kwargs.get('minibatch', 1)
    x_train_original=x_train
    y_target_original=y_target
    if NGW==False:
        w2 = kwargs.get('w2', None)
        w1 = kwargs.get('w1', None)
        b2 = kwargs.get('b2', None)
        b1 = kwargs.get('b1', None)
        w2_init=w2
        w1_init=w1
        b2_init=b2
        b1_init=b1
    mu_inc=10.0                       #This is the factor by which "mu" is multiplied if the error is increasing after a few consecutive interations.
                                #If this factor is too small, the algorithm is no longer converging monotonically. 
                                #If this factor is too big, the algorithm 1) is very slow, 2) might be stuck at a certain performance.
    mu_dec=0.10
    mu=100.0 #First value of mu
    epoch=0
    shuffle=False#True
    restart=False
    old_error=10.
    old_error_test=1000000.
    m=0
    p=0
    g=0
    q=0
    prop=0
    while epoch<epochs :
        if restart == True or epoch==0 : #Can be used to avoid "NAN" errors, if the networks value blows up. This can easily happens as the network is built from exponential neurons.
            epoch=0
            if NGW==True:
                threshold=0.00000001
                ######## NGUYEN - WIDROW ##########
                H=0.7*num_neurons**(1./D)
#                w1=np.array([-1*H,H]) #w1 will be filled with -0.4 or 0.4 randomly
#                w1=w1[np.random.randint(len(w1), size=[num_dimensions,num_neurons])]
                w1=np.random.uniform(low=-1.0*H,high=1.0*H,size=[num_dimensions,num_neurons])
                b1=np.random.uniform(low=-1.0,high=1.0,size=[num_neurons])
#                w2=np.random.uniform(low=-1.0,high=1.0,size=[num_neurons])
                w2=np.zeros((num_neurons))
                w2.fill(1./num_neurons)
                b2=np.zeros((1))
                b2.fill(-1.)
                ######## NGUYEN - WIDROW #END######
            else:
                w1=w1_init
                w2=w2_init
                b1=b1_init
                b2=b2_init

        if batch_number!= 1:
            number_of_elements_in_batch=int(np.floor(len(x_train_original[:,-1])/batch_number))
            x_train=x_train_original[q*number_of_elements_in_batch:(q+1)*number_of_elements_in_batch]
            y_target=y_target_original[q*number_of_elements_in_batch:(q+1)*number_of_elements_in_batch] 
            print(q)
            q+=1
            if q>batch_number:
                q=0


        y_out,y_1,z=apply_network(x_train,w1,b1,w2,b2)
#        print(x_train[5])
#        print(y_out[5])
#        exit()
        y_1=y_1.T
        z=z.T
#        print(np.shape(y_1))
#    print(np.shape(z))
#    print(np.shape(y_out))

#        print(w1,w2,b2,b1)
#        exit()

        error=(np.square(y_target-y_out))   #This is a vector (1D problem) or a matrix (ND problem). For each point, it stores the error.
                                            #The error matrix (vector) should be built for all training training point in order to compute 
                                            #the "gap" vector used in the LM algorithm
        gap=y_target-y_out

#        print(y_target[5],y_out[5],gap[5])


        y_net_test,trash,trash1=apply_network(x_test,w1,b1,w2,b2)
        error_test=(np.square(y_test-y_net_test))        #The followed error is based on the "test" set.
        test=np.sqrt(1./len(y_test)*np.sum(np.square(y_test-y_net_test)))

        error_rmse=np.sqrt(1./len(x_train)*np.sum(error))

        if verbose==True:
            print('error_rmse_train',error_rmse)
            print('error_rmse_test',test)
#    print(np.shape(error))
#    print(np.shape(x_train))
        jacobian=np.zeros((num_neurons*(D+2)+1,len(x_train)))
        B=np.multiply(w2.T[:,np.newaxis],y_1)
        gap=gap
        for k in range(D):    #derivative of network output with respect to w1
            for j in range(num_neurons):
                jacobian[k*num_neurons+j,:]=B[j,:]*x_train.T[k,:]*-1.#*gap
#            print(k*num_neurons+j)
#            print('----')
#    B=(B.T*x_train.T).T
#    print(np.shape(jacobian))
#    print(jacobian)
#    C=B                             
        for j in range(num_neurons):
            jacobian[(D-1)*num_neurons+num_neurons+j,:]=B[j,:]*-1.#*gap#derivative of network output with respect to b1
#        print((D-1)*num_neurons+num_neurons+j)
            jacobian[(D-1)*num_neurons+num_neurons*2+j,:]=y_1[j,:]*-1.#*gap #derivative of network output with respect to w2
            jacobian[(D-1)*num_neurons+num_neurons*3,:]=1.*-1.#*gap
#        for element in jacobian:
#            for elem in element:
#                elem=elem*0.25#print(elem)


#        print(jacobian[0*num_neurons+0,0])
#        w1[0][0]=w1[0][0]-1./200000000.
#        y_out1,y_1,z=apply_network(x_train,w1,b1,w2,b2) 
#        w1[0][0]=w1[0][0]+2./200000000.
#        y_out2,y_1,z=apply_network(x_train,w1,b1,w2,b2)
#        print(0.5*((y_out2[0]-y_target[0])**2.-(y_out1[0]-y_target[0])**2.)*400000000.)#*gap[0])
        


#        exit()
#        hessian=np.matmul(jacobian,jacobian.T)
#        gradient=np.matmul(jacobian,error.T)
#        w1[0][0]=w1[0][0]-1./2000000000. 

        hessian=np.dot(jacobian,jacobian.T)
#        gradient=np.dot(jacobian,error.T)
        gradient=np.dot(jacobian,gap.T)#*error_rmse   WE MULTIPLY BY THE GAP HERE !!



#    print(np.shape(gradient))
#    print(np.shape(hessian))
#        print(hessian)
#    print(error)
#    print("--------")
#        weights_update=np.matmul(np.linalg.inv(hessian+mu*np.identity(len(hessian[:,0]))),gradient)
#        weights_update=np.matmul(np.linalg.inv(hessian+mu*np.dot(error,error.T)*np.identity(len(hessian[:,0]))),gradient)
#        weights_update=np.dot(np.linalg.inv(hessian+mu*np.dot(error,error.T)*np.identity(len(hessian[:,0]))),gradient)
#        weights_update=np.dot(np.linalg.inv(hessian+mu*np.dot(error,error.T)*np.identity(len(hessian[:,0]))),gradient)
#        print(np.dot(error,error.T))
        try:
            HM1=np.linalg.inv(hessian+mu*np.eye(len(hessian[:,0])))
        except np.linalg.LinAlgError:
            print("Not invertible. Go small step in gradient direction")
            weights_update = 1.0/1e10 * gradient
        else:
#            print(HM1)
            weights_update=np.dot(-HM1,gradient)



#        weights_update=np.dot(np.linalg.inv(hessian+mu*np.identity(len(hessian[:,0]))),gradient)
#    print(np.shape(weights_update))
#    print(hessian)
        weigths_network=np.concatenate((w1.flatten(),b1))
        b2_a=np.zeros((1,))
        b2_a[0]=b2
        weigths_network=np.concatenate((weigths_network,w2))
        weigths_network=np.concatenate((weigths_network,b2_a))
        weigths_network_updated=weigths_network+weights_update

        currenterror=np.sum(error)
        if currenterror<old_error:
            p+=1
            m=0
            if p>1: 
                mu=mu*mu_dec
                p=0
        elif currenterror==old_error or m==10:
#            mu=mu
            m=1
            mu=mu*mu_inc
#            print(mu)
#            threshold=threshold/10.
        elif currenterror>old_error and m<10:
            p=0
            m+=1
            mu=mu*mu_inc
            weigths_network_updated=weigths_network
        if currenterror<target_error:
            break
        old_error=currenterror
        if test > old_error_test or test==old_error_test:
            if g>100:    #The optimization stops if the error test set increases 100 iteratitions after the other
                break 
            else:
                g+=1
        else: 
            g=0
        old_error_test=test
#    print('w1,b1,w2,b2')
#    print(weigths_network_updated)
        for i in range(num_neurons):
            b1[i]=weigths_network_updated[(D-1)*num_neurons+num_neurons+i]
            for k in range(D):
                w1[k,i]=weigths_network_updated[k*num_neurons+i]
            b2=weigths_network_updated[-1]
            w2[i]=weigths_network_updated[(D-1)*num_neurons+num_neurons*2+i]


        epoch+=1
        if shuffle:
            unison_shuffled_copies(x_train_original,y_target_original)
#    print('b1')
#    print(b1)
#    print('w1')
#    print(w1)
#    print('b2')
#    print(b2)
#    print('w2')
#    print(w2)
#    exit()



#    if np.sum(error) < old_error:
#        restart=False
#    if error > 10.:
#        restart=True
        if verbose==True:
            print("epoch",epoch)#,w1.flatten())

#OUTPUT

#    x_train_sorted=np.sort(x_train,axis=0)
    y_out,y_1,z=apply_network(x_train,w1,b1,w2,b2)
#error=np.zeros(len(y_out))
    error=np.sqrt(1./len(y_out)*np.sum(np.square(y_target-y_out))) #RMSE OF THE NORMALIZED DATA



    y_net_valid,trash,trash1=apply_network(x_valid,w1,b1,w2,b2)
    validation=np.sqrt(1./len(y_valid)*np.sum(np.square(y_valid-y_net_valid)))


    y_net_test,trash,trash1=apply_network(x_test,w1,b1,w2,b2)
    test=np.sqrt(1./len(y_test)*np.sum(np.square(y_test-y_net_test)))
    
#    for i in range(len(y_net_test)):
#        print(y_test[i],y_net_test[i])   
    if verbose==True:
        print(error)
        print(validation)

    results=[[" ", "Error", "Number of points"],["Training Set",error,x_train.shape[0]],["Validation Set",validation,x_valid.shape[0]],["Test Set",test,x_test.shape[0]],["","Final","Maximum"],["Epochs",epoch,epochs]]
    print("\n","\u0332".join(str(string_network)),":\n")
    print(tabulate(results,tablefmt="fancy_grid"))
    print("Error = {1/(number_points) * sum[(y_set - y_network)^2]}^1/2")



#UNNORMALIZATION OF THE NETWORK PARAMETERS
    for i in range(num_neurons):
        w2[i]=w2[i]*(energy_max-energy_min)/(boundariese[1]-boundariese[0])
        for k in range(D):
            b1[i]+=(w1[k,i])*(boundariesx[0]-(boundariesx[1]-boundariesx[0])*(x_min[k])/(x_max[k]-x_min[k]))
            w1[k,i]=((boundariesx[1]-boundariesx[0])/(x_max[k]-x_min[k]))*w1[k,i] 
    b2=(b2-boundariese[0])*(energy_max-energy_min)/(boundariese[1]-boundariese[0])+energy_min

    return(w2,b2,w1,b1)



# BUILD THE MCTDH OPERATOR
def mctdh_oper(D,Cq,v_shift,bq,weights,filename):
    print("PARAMETER-SECTION",file=open(filename, "a"))
    print("c=",v_shift,file=open(filename, "a"))
    for i in range(len(Cq)):
        print("r",i,"=",Cq[i]*np.exp(bq[i]),file=open(filename, "a"),sep='') #Multiplicative coefficients, Cq*exp(b)
        for j in range(D):
            print("w",i,"u",j,"=",weights[j,i],file=open(filename, "a"),sep='') #Weights arguments of the exponential w as in exp(w)
        
    print("""end-parameter-section
LABELS-SECTION""",file=open(filename, "a"))
    for i in range(len(Cq)):
        for j in range(D):
            print("q",i,"u",j,"=exp[w",i,"u",j,",0,0]",file=open(filename, "a"),sep='')
    print("end-labels-section",file=open(filename, "a"))
    print("HAMILTONIAN-SECTION",file=open(filename, "a"))
    print("-----------------------------------------------",file=open(filename, "a"))
    print("modes       ",end='',file=open(filename, "a"))
    for j in range(D):
        print("|  x_",j,'     ',end='',file=open(filename, "a"),sep='')
    print(" ",file=open(filename, "a"))
    print("-----------------------------------------------",file=open(filename, "a"))
    print("c       ",end='',file=open(filename, "a"))
    for j in range(D):  
        print("|    1 ",end='',file=open(filename, "a"))
    print(" ",file=open(filename, "a"))
    for i in range(len(Cq)):
        print("r",i,end=' ',file=open(filename, "a"),sep='')
        for j in range(D):
            print("|   q",i,"u",j,end='',file=open(filename, "a"),sep='')
        print(" ",file=open(filename, "a"))
    print("-----------------------------------------------",file=open(filename, "a"))
    print("end-hamiltonian-section",file=open(filename, "a"))



def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]


if __name__ == '__main__':
    main()








exit()







