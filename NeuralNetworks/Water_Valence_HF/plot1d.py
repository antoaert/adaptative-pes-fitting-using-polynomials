import yaml
import numpy as np
from potential import *
filename_expansion="HFCO_3D_HF.txt"
expansion = yaml.load(open(filename_expansion,'r'))
import matplotlib.pyplot as plt
#GENERATE A 1D TERM
batchsize=500 #number of points to plot
COORDINATE=5 #int that defines the coordinate that you want to plot
str_term='['+str(COORDINATE)+']'
print(expansion['boundaries_coordinates'])
coord_min=expansion['boundaries_coordinates'][COORDINATE][0]
coord_max=expansion['boundaries_coordinates'][COORDINATE][1]
current_w2=expansion[str_term]['w2']
current_b2=expansion[str_term]['b2']
current_w1=expansion[str_term]['w1']
current_b1=expansion[str_term]['b1']
current_minimas=expansion[str_term]['minimas']
current_coordinates_types=expansion[str_term]['coordinates_types']
x_plot_1D=np.sort(np.random.uniform(low=coord_min, high=coord_max,size=batchsize))
y_out=np.zeros(batchsize)
t=0
for point in x_plot_1D:
  y_out[t]=potential(point,current_w1,current_b1,current_w2,current_b2)[0]
  t+=1
plt.plot(x_plot_1D,y_out)
plt.show()
