import yaml
from constants import *
from basis_function import *
from potential import *
import numpy as np
def total_RMSE(filename_expansion,DataBasePointsPath):
  #COMPUTE THE TOTAL RMSE
  #filename_expansion="Rosenbrock_2D.txt"
  #LOAD THE EXPANSION
  expansion = yaml.load(open(filename_expansion,'r'))
  threshold=expansion['Threshold(cm-1)']/wavenumber_per_ua

  #GET THE POINTS FROM THE DATABASE
  #DataBasePointsPath=str(os.getcwd())+"/"'computed_points/rosenbrock_tab.txt'
  database_data=np.genfromtxt(DataBasePointsPath,dtype=None)
  #The first column is the term, the next D values are the coordinates for a D-coordinates system and the last value is the energy
  all_coordinates=np.zeros((len(database_data),expansion['number_of_dimensions']))
  energies=np.zeros(len(database_data))
  for i in range(len(all_coordinates)):
    for j in range(expansion['number_of_dimensions']):
      all_coordinates[i][j]=database_data[i][j+1]
    energies[i]=database_data[i][int(expansion['number_of_dimensions'])+1]
  all_coordinates=np.delete(all_coordinates,(np.where(energies>threshold)),axis=0)
  energies=np.delete(energies,(np.where(energies>threshold)),axis=0)
  batchsize=len(energies)

  #For each point, we compute the output of the expansion
  output_expansion=np.zeros(batchsize)
  rmse_total=0.0
  #gap=np.zeros(batchsize)
  for z in range(batchsize):
    #For each term:
    for current_term in expansion['full_list_of_terms']:
      #print(current_term)
      if expansion[str(current_term)]['status']!="Optimized": #Do not consider terms that were skipped
        break
      str_term=str(current_term)
      current_order=len(current_term)
      current_w2=expansion[str_term]['w2']
      current_b2=expansion[str_term]['b2']
      current_w1=expansion[str_term]['w1']
      current_b1=expansion[str_term]['b1']
      current_minimas=expansion[str(current_term)]['minimas']
      current_coordinates_types=expansion[str(current_term)]['coordinates_types']
      #What should I supply to the function potential given the term?

      if current_order>1:
        point=np.zeros(current_order)
        for i in range(current_order):
          point[i]=all_coordinates[z][current_term[i]]
      else:
        point=all_coordinates[z][current_term]
      current_y_output=potential(point,current_w1,current_b1,current_w2,current_b2)[0]
      output_expansion[z]+=current_y_output
#      print("Current_y_output",current_term,current_y_output)
    #We want the total RMSE
    rmse_total+=(energies[z]-output_expansion[z])**2.0
  #  print(energies[z],output_expansion[z])
    #gap[z]=(energies[z]-output_expansion[z])**2.0



  rmse_total=rmse_total/len(energies)
  rmse_total=np.sqrt(rmse_total)


  print("Total RMSE of the fit: ",rmse_total," (in a.u.)")
  print("Total RMSE of the fit: ",rmse_total*wavenumber_per_ua," (in cm-1)")
