EnergyThreshold: true
SkipThreshold: 0.001
Threshold(cm-1): 35000.0
VerboseComment: false
WasTheSignChanged: true
'[0, 1, 2]':
  b1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - &id002 !!python/name:numpy.ndarray ''
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [38]
    - &id001 !!python/object/apply:numpy.dtype
      args: [f8, false, true]
      state: !!python/tuple [3, <, null, null, null, -1, -1, 0]
    - false
    - !!binary |
      BGLMK2OyI0CQUqxNPN0lQCGSsngIHBXA4lDae33pF8CZDAO0musGwPjxUcAl7QlAVYqgvzs4IEBw
      Llns/eryP36yMilFFipAhscF+HUoKkAdgknblFgCQEqYQE5CCiVAQoe3GJy1GkBsnfSHBsDwv0zI
      YfGajuU/pjaupSlWB0AWTkPdrAn3v3QFV428sx/AgmKO4L7tJEDQOdaK/KIlQLqENGOvg+8/5S5D
      EBm3CcBUukS9WSIQQKqF+Z3WxBlAadbh3QliFEB/A7usGdsfQHTVIxMsIylApg9IG1dVBED/ZwMH
      r+gTwNTugAq4EShAg68YkCkpCMCVyPXzHwT/P3YKu62LJRVAFv41wQ/l0z8AC6R/GKgqQP8Z20yu
      vgbAUuFjqFcU5T+2iJ/Ue3kZwA==
  b2: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    R3olIEZIhL8=
  coordinates_types: [angle, angle, angle]
  coupling_significance: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    fW8g9a3ijj8=
  fit_points: '192'
  minimas: [1.776705138899756, 1.8557588658852922, 1.7766878101111931]
  number_of_points: '250'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    tjhekmheED8=
  status: Optimized
  valid_points: '58'
  w1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [3, 38]
    - *id001
    - false
    - !!binary |
      RqM6b8+N3D/3l3nudeHYPyrmlWYdNOc/dxpBbq2E57/F8Juq04v7P6j0jNbc/ei/rHhsAy0B6b8F
      9+zSVv7Xv/24K4iYGwfAVPhN82huBcAPUss8NZftvxiCtRqSycM/8acBallT6D/mmZL3DQP2PweX
      Yc5sZOc/snDE8uY2sT8JVGEPx0H0P7Q7fe1QoOo/4iVRrZhcyT/LvtBsEOXXP86fwMnjwPA/vSiM
      plSC4r+mbxdhh+bSv1thTwfOA8+/Ghsqa26w/b/bTFhgiuTtv3cWGP1rogrAV/+p8A6D6T97WEXx
      KlUCQBIpEOl7jg7AScRcj1Hy4r8UWfEKGAblP7Dnh+4Z3ATA6eQ+8pUv0D8v1locHbb9v5wMn9Oe
      M84/UpbPW+fI57+LSKg7yoT2v7VHII65Jg7AkKES3QJlDsA9NOOLHHXcP91wAdq7NgxAQ3x9/ehd
      9T8io9rs4DnbP+lTLpbouwTAZBUhgb+Z1b++gAEaINLFv79eHNdhLMi/1YQLQ7fhtb/81EQl/4oQ
      wJYZw9oIAwfAXbZrrM7L7z8I0FypXkjjPwMMdZspstG/UECuHRrPwb96aiVYZMnjP6cC78vLPBDA
      CtqOWlnlDsDxD9tOmGqSP9RIsLTxXAFAJ7lfWLRl8b8Fx8IWlogFwIQilRda/OA/7W6nXIHYAcAj
      jo8HU+i9v+G3mrp+6H8/8QEfGsWl4b86WTW/Ltamv6KvBVe5QwBAL/6FF8SRi7+akk44sW/lP2/Z
      ENBPatu/BLYXTc+Jyb+KknKgaz/OP1nIKPutFuG/6KqR6g/iEkDKoFSorvLyv6kZnq/fDvu/QPYP
      8usc+j8f5ySZ1unWPwhNvh64XPW/VhVH8Kb02r9qZAj+HkTWv21EpeGgo86/kadOqmkcBMDP/Tqq
      FBAGwClYIkCKVMw/J/p2BEJs7b+TmvkNjsz0v82eyItw7PK/WzTisfdF6b8MAC/VMZSov75XoFa0
      U8g/iGPB0SlgAkCje6vkyEHrvwdP0ae0NPi/Eq/oXy/N8L+yVRwlT7DnPy3kHcpATbs/n+OmTGJT
      u79Ir6cVkMHXv0LasQlEYdq/Km5HDtfYAMABv5bQiWwAwKH+NgsqXuI/OljsNYfi/L/GYB61r8Dp
      Py6BYRjPp9C/1XafYyl41r8nHwf1jwPdv+zy4dAYThDAjKgbgFnT4T+h0J1tiJbqPwmVzgXiVqS/
  w2: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [38]
    - *id001
    - false
    - !!binary |
      pVWDdPLBiT8xNOTLaCmEP1VDnRfnJo2/kyt6hivTaL+fuj89lbB+P8be1b7+15e//s1rWyF+j7+1
      LeEGIlFJPyBvCD8YYpE/zy1R5tXAhb9HDWitW5mMv9ZNt2GhqpI/CAoVmKjUgL/8dC9O7VuNvyhY
      vApP2pg/YPGVgaWBoT8N6DeENWCWPxH5I+0DdoE/v4MMOc5ckL9M3Tiw67WRv9acQBAxo5I/vrgX
      EYr6ij/4zEfjeliavzGswZxjspA/hMLSYIRLij+cmmGXxUCLP0waBV4fX4+/xTe9bvmKfL/VTl7y
      Vodyv5EoN+ziVoE/YGUffwsQh78spCDWlq2avzxbjoAHXIK/J2ONmFH+Vj/oINaX/O9AP8Mz1f0J
      wYI/MRTAN3aqjj83lvuj5rUwPw==
'[0, 1]':
  b1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [13]
    - *id001
    - false
    - !!binary |
      eegfx2fYAkD39beswwwRQP5C9tdS9xRAnIOgD/dqE0B9ECWxJzoYQBK3f8Z3Le8/+Tbbn3sf8z/O
      xG9idkMXQLBAdtmM5tc/wAOtAZjjEkAQveSrYTfXP7D80qdccQtA5n2c8N8q+D8=
  b2: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    +X0yeJDqyr8=
  coordinates_types: [angle, angle]
  coupling_significance: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    mdEiDACYtj8=
  fit_points: '52'
  minimas: [1.776705138899756, 1.8557588658852922]
  number_of_points: '70'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    CPELvpENFD8=
  status: Optimized
  valid_points: '18'
  w1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [2, 13]
    - *id001
    - false
    - !!binary |
      y5f2YJNE6r9kI/e9QTP2vyauVdH1Mtc/ppMt7tNf/b8KLLZgknjTPyX3k19S9te/caMco+gI3L/h
      ei20pzXSP/gl2/FDuOy/5tBaEozu0D8dflIf7Gfxvzzf1SIzFvI/LHfxdGwoz79twzg7Qz7MPyd9
      QHEry9G/IZl3CUiLBcBOVRpqQVvgv50Ypqo1GQLAp1gOK79gzj8DnDuCGp/iPydvVcggwP2/16aU
      7Q2/8j92QOMkNA34vxdHv4umwfM/+gJLL1/fCMDMbl4okfrkvw==
  w2: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [13]
    - *id001
    - false
    - !!binary |
      2nYRIeSws7/K46KMt6qmP5j+8NmTW7O/tvW/ocgSm79dwpR0sSO1Pxw6lnWDeKY/j1jCnm/arj+2
      Oz7U50G3v70L6a61JK+/d/aGiXBjtj+9/M9WBrKoP0f59SrN82o/ISQT2cSLhb8=
'[0, 2]':
  b1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [5]
    - *id001
    - false
    - !!binary |
      wEPU0QiHBUA7B+Fxq98cQI5zZ78n4T9ASZMPVa9UHEDG0G6Dac39Pw==
  b2: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    MI3EpYiY2L8=
  coordinates_types: [angle, angle]
  coupling_significance: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    EJtPLoxUgT8=
  fit_points: '22'
  minimas: [1.776705138899756, 1.7766878101111931]
  number_of_points: '29'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    Ayj93j3NAz8=
  status: Optimized
  valid_points: '7'
  w1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [2, 5]
    - *id001
    - false
    - !!binary |
      KWqOy5CYA0DDenqFZ+O7Pwxkb2vfUybAz+1XG7VnvD//CVlLd2zxv5l6AjzAFhDA7y/sbL6Rpj/k
      NxnI9n0QwMjSP60KEqc/G+BUaZVI1T8=
  w2: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [5]
    - *id001
    - false
    - !!binary |
      EsaYMdP8BT/OQGEFXF+FP2muHuXTukW+Ourz27Hah7/FE319DuBkPw==
'[0]':
  b1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [3]
    - &id003 !!python/object/apply:numpy.dtype
      args: [f8, false, true]
      state: !!python/tuple [3, <, null, null, null, -1, -1, 0]
    - false
    - !!binary |
      6zTPWEfCCkAYjRf2dKUdQBnW48vC7AxA
  b2: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    Yh1HJAVBBMA=
  coordinates_types: [angle]
  coupling_significance: 1.0
  fit_points: '30'
  minimas: [1.776705138899756]
  number_of_points: '41'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    cJRIp4WI/z4=
  status: Optimized
  valid_points: '11'
  w1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [1, 3]
    - *id003
    - false
    - !!binary |
      /iyQkwZVzT/sGIS2ETgLwPY/mDpUDMs/
  w2: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [3]
    - *id003
    - false
    - !!binary |
      abJAlNqq27/LUwetBMeNP+ngp2w5ttg/
'[1, 2]':
  b1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [12]
    - *id001
    - false
    - !!binary |
      8c/WkGJRD8BqLn7R0l8FwB1EELgJGhTAw0PJskoFJEASP52j2hoiQIPd1ASQWAPA5FQ7VdxF8T/E
      x1KEOe3dv4yyEszuciFAmBo6vR+Y3T+OcLWRw1nxv0JSSW1C0w7A
  b2: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    1eOHXq/Err8=
  coordinates_types: [angle, angle]
  coupling_significance: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    WCazMrdJtz8=
  fit_points: '48'
  minimas: [1.8557588658852922, 1.7766878101111931]
  number_of_points: '66'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    vAlSKhu8Ej8=
  status: Optimized
  valid_points: '18'
  w1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [2, 12]
    - *id001
    - false
    - !!binary |
      rs1kwVPJ8j+B1YvmIzDjP4V5xWe8Xfg/L7ZS1aboCsBkIS8dmqgKwPWL/1CDveM/KOouEqBxzr9M
      gLUlk2HzP3TTGIHLqArAXOuIv2W29j8sdodbRY3dv/B22E2D4PM/kzBmSuJC8z/h266BU2XyPwuf
      Hg4cofY/YjdYEXS19b+EKQjnD1fwv9AnR7aYZds/Mb4U1dbQwL8K1wKwHXLjvydCVlhdhe2/V9dj
      cqNH8L+8aXHbFJroPyzBfFgalec/
  w2: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [12]
    - *id001
    - false
    - !!binary |
      3oEfxRZrsb/kkNLkAdakP/Ox7v8Qh5I/WGM4KpT2eb83As1FneemP8OarPNlsJo/qO7xDzeSpD+N
      uTj0V8GOv64XZXi0VaW/ZMB14LcpaT/hwPBQwfmsv1XOnmKAXa0/
'[1]':
  b1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [7]
    - *id003
    - false
    - !!binary |
      Uc5dEUA4FkApdthZG0EhQLpPb8RqpB1Arkdv2MoGIUCkHDPVvlsCQHcIBml6SR9AsB1jOvL+qz8=
  b2: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    9tuj6ygfB8A=
  coordinates_types: [angle]
  coupling_significance: 1.0
  fit_points: '28'
  minimas: [1.8557588658852922]
  number_of_points: '40'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    0TzuDfIQFD8=
  status: Optimized
  valid_points: '12'
  w1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [1, 7]
    - *id003
    - false
    - !!binary |
      K57dZM6a+b8/G0qRVrXnv+GogkCb4vG/vLM80GkY7L+iA2fqyk3rv4NAYWhzj+S/vaZ7DwY/4b8=
  w2: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [7]
    - *id003
    - false
    - !!binary |
      xT3YhzOg0j8S42AXn2Dev0Sys+eFv9q/T3IuTIdE3D+MInN5zv68P1xD5L5JaNs/y0zd1Mnlnr8=
'[2]':
  b1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [3]
    - *id001
    - false
    - !!binary |
      eR1UGh+mCkDMWQOFK+AeQMqgGDygeg1A
  b2: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    C/wYWb5wA8A=
  coordinates_types: [angle]
  coupling_significance: 1.0
  fit_points: '30'
  minimas: [1.7766878101111931]
  number_of_points: '41'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    OpZUmzcFAD8=
  status: Optimized
  valid_points: '11'
  w1: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [1, 3]
    - *id001
    - false
    - !!binary |
      2LerTVEFzj9+c1Ky/D4LwK+M+iNV2Ms/
  w2: !!python/object/apply:numpy.core.multiarray._reconstruct
    args:
    - *id002
    - !!python/tuple [0]
    - !!binary |
      Yg==
    state: !!python/tuple
    - 1
    - !!python/tuple [3]
    - *id001
    - false
    - !!binary |
      m4a/yrid3L+y+e0QUfSFPzJqCPHCS9c/
boundaries_coordinates:
- &id004 !!python/tuple [1.25, 2.95]
- !!python/tuple [1.0, 2.65]
- *id004
full_list_of_terms:
- [0]
- [1]
- [2]
- [0, 1]
- [0, 2]
- [1, 2]
- [0, 1, 2]
local_bounds:
- *id004
- !!python/tuple [0.8, 3.2]
- *id004
max_order: 3
minimum_coordinates:
- [1.776705138899756, 1.8557588658852922, 1.7766878101111931]
- [-76.065907410497]
name_of_coordinates: [ROH1, AHOH, ROH2]
number_of_dimensions: 3
rmse_target_fit: 5.4676023249985605e-05
rmse_target_newpoints: 9.112670541664268e-05
type_of_coordinates: [angle, angle, angle]
