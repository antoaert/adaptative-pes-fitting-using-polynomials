This project is in current development, its final form might not ressemble its current state.

A tentative manual is on Overleaf : https://www.overleaf.com/read/vkvcchzmwrtq

This code relies on the work of others! Cite:
@misc{Nijholt2019,
  doi = {10.5281/zenodo.1182437},
  author = {Bas Nijholt and Joseph Weston and Jorn Hoofwijk and Anton Akhmerov},
  title = {\textit{Adaptive}: parallel active learning of mathematical functions},
  publisher = {Zenodo},
  year = {2019}
}

@article{jcp_156_164106,
doi= {10.1063/5.0089570},
author = {Aerts , Antoine and Sch\"afer, Moritz R.  and Brown , Alex },
title = {Adaptive fitting of potential energy surfaces of small to medium-sized molecules in sum-of-product form: Application to vibrational spectroscopy},
journal = {J. Chem. Phys.,
volume = {156},
number = {16},
pages = {164106},
year = {2022}
}
