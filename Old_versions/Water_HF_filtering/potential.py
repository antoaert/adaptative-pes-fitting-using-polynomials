import numpy as np
from basis_function import *
def potential(point,order,coordinates_types,functional_sequence,alpha_parameters,xeq,expansion_parameters):
  if order==1:
   point=[point]
  energy=0.
  derivatives=np.zeros(len(expansion_parameters))
  for j in range(len(expansion_parameters)): #sum over the product of the basis functionals 
    basis=1. #products of basis functionals
    for i in range(order):
      if coordinates_types[i]=='stretch':
        basis=basis*exp1(point[i],xeq[i],alpha_parameters[i],functional_sequence[j][i])
      elif coordinates_types[i]=='angle':
        basis=basis*q1(point[i],xeq[i],alpha_parameters[i],functional_sequence[j][i])
      elif coordinates_types[i]=='angletor':
        if (functional_sequence[j][i])<0.:
          basis=basis*sin1q2(point[i],xeq[i],abs(functional_sequence[j][i]))
        else:
          basis=basis*cosSq2(point[i],xeq[i],functional_sequence[j][i])
      elif coordinates_types[i]=='torsion':
        if (functional_sequence[j][i])<0.:
          basis=basis*sin1(point[i],xeq[i],abs(functional_sequence[j][i]))
        else:
          basis=basis*cosS(point[i],xeq[i],functional_sequence[j][i])
    energy=energy+expansion_parameters[j]*basis
    derivatives[j]=basis
  return energy,derivatives
def potential_lm(point,*expansion_parameters,order,coordinates_types,functional_sequence,alpha_parameters,xeq):
#  print(point)
#  print(expansion_parameters)
  if order==1:
   point=[point]
  energy=0.
  derivatives=np.zeros(len(expansion_parameters))
  for j in range(len(expansion_parameters)): #sum over the product of the basis functionals 
    basis=1. #products of basis functionals
    for i in range(order):
      if coordinates_types[i]=='stretch':
        basis=basis*exp1(point[i],xeq[i],alpha_parameters[i],functional_sequence[j][i])
      elif coordinates_types[i]=='angle':
        basis=basis*q1(point[i],xeq[i],alpha_parameters[i],functional_sequence[j][i])
      elif coordinates_types[i]=='angletor':
        if (functional_sequence[j][i])<0.:
          basis=basis*sin1q2(point[i],xeq[i],abs(functional_sequence[j][i]))
        else:
          basis=basis*cosSq2(point[i],xeq[i],functional_sequence[j][i])
      elif coordinates_types[i]=='torsion':
        if (functional_sequence[j][i])<0.:
          basis=basis*sin1(point[i],xeq[i],abs(functional_sequence[j][i]))
        else:
          basis=basis*cosS(point[i],xeq[i],functional_sequence[j][i])
    energy=energy+expansion_parameters[j]*basis
  return energy
