#!bin/python
import os
import numpy as np
from constants import *
import subprocess
import uuid
from jinja2 import Template

def my_function_to_learn(xy,current_term,minimum_coordinates): #MOLPRO ND HFCO
  if len(current_term)==1:
    xy=[xy]
  data_base_folder=str(os.getcwd())+"/"+"computed_points/"
  if not os.path.exists(data_base_folder):
    os.mkdir(data_base_folder)

  #equi_geometry=[1.308,1.83,1.320,30.5,104.5,180.0] #Define first the equilibrium geometry
  geometry=np.zeros(len(minimum_coordinates[0]))
  for i in range(len(minimum_coordinates[0])): #From the number of arguments (len of the list given by adaptative), assign 
    if i in current_term: #->geometry coordinates to the term element (current_term). 
#      print(np.where(np.array(current_term)==i)[0][0])
      geometry[i]=xy[np.where(np.array(current_term)==i)[0][0]]
    else:
      geometry[i]=minimum_coordinates[0][i]

  #There is a MAPPING of the coordinates to do! 
  #Reminder: [RCO,RCH,RCF,AHOC,AFOC,DFHOC]
  #The valence coordinates: [RCO{BOHR},RCH{BOHR},RCF{BOHR},cos(AHOC{RAD}),cos(AHOC{RAD}},DFHOC{RAD}]
  #The procedure will provide the lengths in Bohr, molpro will use them in angstrom
  geometry_mapped=np.zeros(len(geometry))
  geometry_mapped[0]=geometry[0]/angstrom_to_bohr
  geometry_mapped[1]=geometry[2]/angstrom_to_bohr
  geometry_mapped[2]=geometry[1]/radian_per_degree

  #Use this list that define the geometry in the coordinates (and units) of the z-matrix in a template of the molpro calculation. 
  template_molpro_hfco='''***, H2O aug-cc-pVTZ-F12 uks
  memory,250,m   
  print orbitals
  geometry={angstrom
  H
  O 1 ROH1
  H 2 ROH2 1 AHOH}
 
  basis=cc-pVTZ-f12
  ROH1={{r_oh1}}
  ROH2={{r_oh2}}
  AHOH={{a_hoh}}
  hf;
  escf=energy
  ccsd(t)-f12;
  eccsdta=energy(1)
  eccsdtb=energy(2)
  '''
  tm=Template(template_molpro_hfco)

  input_file=tm.render(
  r_oh1=geometry_mapped[0],
  r_oh2=geometry_mapped[1],
  a_hoh=geometry_mapped[2])

  #print(input_file)
  unique_filename = str(uuid.uuid4())
  directory_path=str(os.getcwd())+"/"+"hfco"+unique_filename+"/"
  if not os.path.exists(directory_path):
    os.mkdir(directory_path)

  input_file_path=directory_path+"hfco"+".inp"
  with open(input_file_path, 'w') as f:
     print(input_file, file=f)
  f.close()

  #Once the input file is created, run the program
  exit_status=os.system("molpro "+str(input_file_path))
  
  if exit_status!=0:
    print("The molpro command failed")
    energy=10000000.0
    energy1=0.0  
  else:
    #Once this is over, get what you are interested in from the output
    proc = subprocess.Popen('grep -r "SETTING ESCF" '+str(directory_path)+'''hfco.out | head -1 | cut -d "=" -f 2 | tr -s ' '| cut -d ' ' -f 2 | head -n 1''', stdout=subprocess.PIPE, shell=True)
    energy=proc.communicate()[0]
    proc1=subprocess.Popen('grep -r "SETTING ECCSDTA" '+str(directory_path)+'''hfco.out | head -1 | cut -d "=" -f 2 | tr -s ' '| cut -d ' ' -f 2 | head -n 1''', stdout=subprocess.PIPE, shell=True)
    energy1=proc1.communicate()[0]
    #Keep it somewhere
    path_file_db=data_base_folder+"hfco_tab.txt"
    newline=str(current_term).replace(' ','')+' '
    for i in range(len(geometry_mapped)):
      newline+="{:.8f}".format(geometry_mapped[i])+' '
    newline+="{:.8f}".format(float(energy))+' '
    newline+="{:.8f}".format(float(energy1))+' '
    print(newline,file=open(path_file_db,"a"))

    path_file_db_mapped=data_base_folder+"hfco_tab_mapped.txt"
    newline=str(current_term).replace(' ','')+' '
    for i in range(len(geometry)):
      newline+="{:.8f}".format(geometry[i])+' '
    newline+="{:.8f}".format(float(energy1)-float(energy)-minimum_coordinates[1][0])+' '
    print(newline,file=open(path_file_db_mapped,"a"))
  
  os.system("rm -rf "+str(directory_path)) 
  
  
  return {'energ':float(energy1)-float(energy)-minimum_coordinates[1][0],'scf':float(energy)-minimum_coordinates[1][1]} #ATTENTION RETURN THE OPPOSITE OF THE VALUE IF YOU WANT THE SAMPLING TO BE BETTER AROUND THE MINIMUM RATHER THAN THE MAXIMUM OF THE ENERGY!!
                                          #WE PROBABLY DON'T WANT THAT FOR THE CORRELATION
