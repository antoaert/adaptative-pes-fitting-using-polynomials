#Iteration tools
from itertools import combinations,permutations

#some math
import numpy as np 
import math

#Plotting:
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

#Create input file from template
from jinja2 import Template

#Sampling of the points:
import adaptive

#Read/store expansion
import json,pickle,yaml

#Database
import os
import subprocess

#Random file name 
import uuid

#Other python files
from constants import *
from basis_function import *
from potential import * 
from training_poly import *
from useful_warnings import *
from useful_functions import *
from build_expansion import *
from optimization_expansion import *
from my_function_to_learn import *



from concurrent.futures import ProcessPoolExecutor

#####PARAMETERS OF YOU JOB ########
nCores=4
name_of_the_expansion='HF'
number_of_dimensions=3
max_order=3
name_of_coordinates=['ROH1','AHOH','ROH2']    #Valence corrdinates
type_of_coordinates=['angle','angle','angle'] #The basis set of functions depends on the type of coordinates that define the system


'''------>  ACTUAL POSSIBLE CHOICES : angle, torsion or stretch
            IF THE DIHEDRAL ANGLE GOES THROUGH THE WHOLE PERIOD, USE 'TORSION', IF NOT USE 'ANGLE'
'''



#INSIGHT ABOUT THE DATA
#GLOBAL MINIMUM: !!! IN VALENCE COORDINATES !!!
minimum_coordinates=[[0.94019187*angstrom_to_bohr,106.32710524*radian_per_degree,0.94018270*angstrom_to_bohr],[-76.065907410497]] #[[x,y],[z]]
#minimum_coordinates=[[0.0,0.0],[0.0]]

#CHOSEN BOUNDARIES
boundaries_coordinates=[(1.25,2.950),(1.00,2.65),(1.25,2.950)]
local_bounds=[(1.25,2.950),(0.80,3.20),(1.25,2.950)]
local_energy_threshold=35000.
#boundaries_coordinates=[(-1.12,4.12),(-1.12,4.12)]

#MAXIMUM ENERGY THAT SHOULD BE CONSIDERED IN CM-1
EnergyThreshold=True
if EnergyThreshold:
    energy_threshold=35000.
    energy_threshold=energy_threshold/wavenumber_per_ua
else:
    energy_threshold=1.e12

#SIZE OF THE VALIDATION SET, AS A FRACTION OF THE FITTING SET SIZE, E.G. 0.2 IS 20%
validation_frac=0.3 



#SequentialOptimization=True #Only for testing, not recommended
SequentialOptimization=False #Recommended
rmse_target_newpoints=5.0/wavenumber_per_ua #5cm-1
rmse_target_fit=rmse_target_newpoints*0.8

APrioriSkip=True
skip_threshold=1e-3
''' -----> This feature will compute a coupling significance, and skip in advance some supposed unsignificant terms'''
skip=[[]]#[[1],[2],[4],[5],[0,1],[0,2]]#[[0],[1]] #Ex: [[0,1],[0]]  
 #Add the ability to skip some terms. If there is too much corelation 
 #between two coordinates, then it might be beneficial to go to second order 
 #terms for them directly. Examples include molecules with multiple isomers.
 #ATTENTION ! MUST START COUNTING FROM ZERO AS PYTHON DOES

DoISaveTheExpansion=True
if DoISaveTheExpansion:
  filename_expansion="H2O_3D_HF.txt"

#IsThisARestart=False
IsThisARestart=True
DataBasePointsPath=str(os.getcwd())+"/"'computed_points/h2o_tab_mapped.txt' 
'''--> It has to match the dabase file of the my_function_to_learn !'''

Verbose_comment=False#True
WasTheSignChanged=True




###USEFUL WARNINGS
warnings(IsThisARestart,DataBasePointsPath,number_of_dimensions,max_order,minimum_coordinates)
###END WARNINGS


#We build a dictionary that defines the expansion.
#The expansion is defined by a number of terms, each of which acts on one or 

#multiple coordinates. Each of the term is build from a sum of products of 
#functional, the type of functions differ from the type of coordinate that it
#needs to fit. We fit the data with increasing order of the terms (1D first ...)
#and the sum of product shall follow an increasing order of the functional 
#(q^2 + q^3 + q^4 ...) in 1D for example. 


initialize_expansion(filename_expansion,IsThisARestart,number_of_dimensions,max_order,name_of_coordinates,type_of_coordinates,boundaries_coordinates,local_bounds,minimum_coordinates,skip,skip_threshold,rmse_target_fit,rmse_target_newpoints,EnergyThreshold,energy_threshold,WasTheSignChanged,DoISaveTheExpansion,Verbose_comment)



#WE CONSTRUCT THE EXPANSION BY COMPUTING POINTS AND FITTING OF COEFFICIENTS


construct_expansion(filename_expansion,DoISaveTheExpansion,IsThisARestart,DataBasePointsPath,SequentialOptimization,Verbose_comment,local_energy_threshold,validation_frac,nCores)








#COMPUTE THE TOTAL RMSE
#total_RMSE(filename_expansion,DataBasePointsPath)

