import numpy as np
#definition of the functions basis set

##FOR STRETCH COORDINATES:
def exp1(x,xeq,alpha,k):
  return (1-np.exp(-1.0*alpha*(x-xeq)))**k
def alpha_exp1(x_min,xeq):
  return ((-0.593147)/(x_min-xeq))
#  return ((-0.693147)/(x_min-xeq))

##FOR ANGLE COORDINATES
def q1(x,xeq,alpha,k):
  return ((alpha*(x-xeq)))**k
def alpha_q1(x_min,x_max,xeq):
  a=1./(x_max-xeq)
  b=1./(xeq-x_min)
#  if a>b:
#    alpha=b
#  else:
#    alpha=a
  alpha=(a+b)/2.
  return alpha

##FOR TORSION ANGLE COORDINATES
def sin1(x,x_eq,k):
  return (np.sin(k*(x-x_eq)))
def cosS(x,x_eq,k):
  return (1.-np.cos(k*(x-x_eq)))
#These functions are already normalized

def cosSq2(x,x_eq,k):
  return ((x-x_eq)**2.)*(np.cos(k*(x-x_eq)))
def sin1q2(x,x_eq,k):
  return ((x-x_eq)**2.)*(1.0-np.sin(k*(x-x_eq)))




def morse(x,q,p,r,s,m,xeq):
    return (q * (1-np.exp(-1.*m*(x-xeq)))**2.)+(p * (1-np.exp(-1.*m*(x-xeq)))**3.)+(r * (1-np.exp(-1.*m*(x-xeq)))**4.)+(s * (1-np.exp(-1.*m*(x-xeq)))**5.)


def harm(x,q,p,r,s,m,xeq):
    return(q*(m*(x-xeq))**2.+p*(m*(x-xeq))**3.+r*(m*(x-xeq))**4.+s*(m*(x-xeq))**5.)
#    return(np.sqrt(q*q)+np.sqrt(p*p)+np.sqrt(m*m)+q*(m*(x-xeq))**2.+p*(m*(x-xeq))**3.)#+r*(m*(x-xeq))**4.+s*(m*(x-xeq))**5.)
#def harm(x,q,p,m,xeq):
#    return(((q+p)*m)**2.+q*(m*(x-xeq))**2.+p*(m*(x-xeq))**3.)#+r*(m*(x-xeq))**4.+s*(m*(x-xeq))**5.)
