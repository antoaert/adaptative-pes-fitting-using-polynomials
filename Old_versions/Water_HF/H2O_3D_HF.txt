EnergyThreshold: true
SkipThreshold: 0.001
Threshold(cm-1): 35000.0
VerboseComment: false
WasTheSignChanged: true
'[0, 1, 2]':
  coordinates_types: [angle, angle, angle]
  coupling_significance: !!python/object/apply:numpy.core.multiarray.scalar
  - &id001 !!python/object/apply:numpy.dtype
    args: [f8, false, true]
    state: !!python/tuple [3, <, null, null, null, -1, -1, 0]
  - !!binary |
    mrGzrVF8eD8=
  expansion_coeff:
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      9+8DxnJLor8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      e2drcInNir8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      KdnNAtosnj8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      buTSNBilsz8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      3TUZOVWMeD8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      8sqEy+zQkr8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      uWWdm0Kis78=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      fMfEX4WNfT8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      z44fK10zjb8=
  fit_points: '23'
  functional_sequence:
  - [1, 1, 1]
  - [3, 3, 1]
  - [1, 2, 1]
  - [2, 3, 3]
  - [2, 1, 1]
  - [1, 2, 2]
  - [2, 3, 2]
  - [1, 1, 2]
  - [2, 2, 1]
  minimas: [1.776705138899756, 1.8557588658852922, 1.7766878101111931]
  number_of_points: '30'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    GnBESe49ED8=
  status: Optimized
  valid_points: '7'
'[0, 1]':
  coordinates_types: [angle, angle]
  coupling_significance: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    gCUp6nv/uT8=
  expansion_coeff:
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      vfKS4RYHoD8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      qWA7nzoMor8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      VX9bVCOCUD8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      B7vuWAeXmb8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      pc1uznCGpT8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      iUbuzEbmdT8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      flvLUkZLkr8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      0/Pb9T6xMb8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      S77L/3afcb8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      kJFQr6ZqdT8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      29Tao3B/nz8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      eTao7uxJUL8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      tz3AlqZCcb8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      JW+SgQXNWL8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      RR0v+UWdlD8=
  fit_points: '47'
  functional_sequence:
  - [1, 1]
  - [1, 4]
  - [4, 1]
  - [1, 2]
  - [1, 3]
  - [4, 3]
  - [3, 3]
  - [2, 4]
  - [3, 1]
  - [5, 5]
  - [2, 5]
  - [3, 2]
  - [2, 2]
  - [2, 1]
  - [3, 4]
  minimas: [1.776705138899756, 1.8557588658852922]
  number_of_points: '65'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    f7rIXkIFCz8=
  status: Optimized
  valid_points: '18'
'[0, 2]':
  coordinates_types: [angle, angle]
  coupling_significance: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    c7yodjZjZT8=
  expansion_coeff:
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      Y5NZxfwbar8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      Od+WUGEXUT8=
  fit_points: '9'
  functional_sequence:
  - [1, 1]
  - [2, 2]
  minimas: [1.776705138899756, 1.7766878101111931]
  number_of_points: '14'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    pa6rOwVl8j4=
  status: Optimized
  valid_points: '5'
'[0]':
  coordinates_types: [angle]
  coupling_significance: 1.0
  expansion_coeff:
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      bN46Cajxxz8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      +wb40Ckbxr8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      FQuBvz83vD8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      ZltNvSptr78=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      /eMkgI9IoD8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      SoqAW5hIib8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      VoipjAOHYj8=
  fit_points: '20'
  functional_sequence:
  - [2]
  - [3]
  - [4]
  - [5]
  - [6]
  - [7]
  - [8]
  minimas: [1.776705138899756]
  number_of_points: '28'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    /8z1i1BM4j4=
  status: Optimized
  valid_points: '8'
'[1, 2]':
  coordinates_types: [angle, angle]
  coupling_significance: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    ubP0fFEhvj8=
  expansion_coeff:
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      9fERaSuGpT8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      wRqTkWmOoj8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      PPf3LBQiZ78=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      pTGD8ER0ob8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      atqsHrQVir8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      7PZ8/g+YZr8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      Q5mv5e7Tkr8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      cbhgX6byfb8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      jmIiDEh+pb8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      Lbe+kEsUkD8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      7+qsMOoScj8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      UE2u+2fahb8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      2ZGx10ieZD8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      bToRQM7hrD8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      r39BWj8MpT8=
  fit_points: '65'
  functional_sequence:
  - [1, 1]
  - [3, 1]
  - [4, 2]
  - [2, 1]
  - [3, 3]
  - [1, 2]
  - [4, 4]
  - [2, 2]
  - [4, 1]
  - [3, 2]
  - [5, 5]
  - [1, 3]
  - [1, 5]
  - [4, 3]
  - [5, 1]
  minimas: [1.8557588658852922, 1.7766878101111931]
  number_of_points: '87'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    IC7vS96CAj8=
  status: Optimized
  valid_points: '22'
'[1]':
  coordinates_types: [angle]
  coupling_significance: 1.0
  expansion_coeff:
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      crpxvmiyxj8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      BgxUhZXOtb8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      FeI2CLt4n78=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      wulIG9OQkL8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      9drjCTtJqr8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      smWfMjQmfj8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      gStNsURwwD8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      7CWjJ0hmtL8=
  fit_points: '20'
  functional_sequence:
  - [2]
  - [3]
  - [4]
  - [5]
  - [6]
  - [7]
  - [8]
  - [9]
  minimas: [1.8557588658852922]
  number_of_points: '28'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    jb/NL/Ch7D4=
  status: Optimized
  valid_points: '8'
'[2]':
  coordinates_types: [angle]
  coupling_significance: 1.0
  expansion_coeff:
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      4ZFNLjSF1D8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      tYzMbB/P2L8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      KR5ynbC41D8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      PoHKAJErzr8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      ++MDsnp6xD8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      KIYVVqXntL8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      O6a+jcQplD8=
  fit_points: '20'
  functional_sequence:
  - [2]
  - [3]
  - [4]
  - [5]
  - [6]
  - [7]
  - [8]
  minimas: [1.7766878101111931]
  number_of_points: '28'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    DYolqYtp4z4=
  status: Optimized
  valid_points: '8'
alpha_basis: !!python/object/apply:numpy.core.multiarray._reconstruct
  args:
  - !!python/name:numpy.ndarray ''
  - !!python/tuple [0]
  - !!binary |
    Yg==
  state: !!python/tuple
  - 1
  - !!python/tuple [3]
  - !!python/object/apply:numpy.dtype
    args: [f8, false, true]
    state: !!python/tuple [3, <, null, null, null, -1, -1, 0]
  - false
  - !!binary |
    enNqHK239D9wkfYHBUbmPwVx4vtupu8/
boundaries_coordinates:
- &id002 !!python/tuple [1.25, 2.95]
- !!python/tuple [1.0, 2.65]
- *id002
full_list_of_terms:
- [0]
- [1]
- [2]
- [0, 1]
- [0, 2]
- [1, 2]
- [0, 1, 2]
local_bounds:
- *id002
- !!python/tuple [0.8, 3.2]
- *id002
max_order: 3
minimum_coordinates:
- [1.776705138899756, 1.8557588658852922, 1.7766878101111931]
- [-76.065907410497]
name_of_coordinates: [ROH1, AHOH, ROH2]
number_of_dimensions: 3
rmse_target_fit: 1.822534108332854e-05
rmse_target_newpoints: 2.278167635416067e-05
type_of_coordinates: [angle, angle, angle]
