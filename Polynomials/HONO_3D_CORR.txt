EnergyThreshold: true
SkipThreshold: 0.002
Threshold(cm-1): 10000.0
VerboseComment: false
WasTheSignChanged: true
'[0, 1, 2, 3, 4, 5]':
  coordinates_types: [stretch, stretch, stretch, angle, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, 1.8443726976347516, -0.3945842693678343,
    -0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 2, 3, 4]':
  coordinates_types: [stretch, stretch, stretch, angle, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, 1.8443726976347516, -0.3945842693678343,
    -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 2, 3, 5]':
  coordinates_types: [stretch, stretch, stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, 1.8443726976347516, -0.3945842693678343,
    0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 2, 3]':
  coordinates_types: [stretch, stretch, stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, 1.8443726976347516, -0.3945842693678343]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 2, 4, 5]':
  coordinates_types: [stretch, stretch, stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, 1.8443726976347516, -0.25595270845356366,
    0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 2, 4]':
  coordinates_types: [stretch, stretch, stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, 1.8443726976347516, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 2, 5]':
  coordinates_types: [stretch, stretch, stretch, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, 1.8443726976347516, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 2]':
  coordinates_types: [stretch, stretch, stretch]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, 1.8443726976347516]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 3, 4, 5]':
  coordinates_types: [stretch, stretch, angle, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, -0.3945842693678343, -0.25595270845356366,
    0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 3, 4]':
  coordinates_types: [stretch, stretch, angle, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, -0.3945842693678343, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 3, 5]':
  coordinates_types: [stretch, stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, -0.3945842693678343, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 3]':
  coordinates_types: [stretch, stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, -0.3945842693678343]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 4, 5]':
  coordinates_types: [stretch, stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, -0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 4]':
  coordinates_types: [stretch, stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1, 5]':
  coordinates_types: [stretch, stretch, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.235546005432286, 2.6172706826066916, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 1]':
  coordinates_types: [stretch, stretch]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [2.235546005432286, 2.6172706826066916]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 2, 3, 4, 5]':
  coordinates_types: [stretch, stretch, angle, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1, 1]
  minimas: [2.235546005432286, 1.8443726976347516, -0.3945842693678343, -0.25595270845356366,
    0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 2, 3, 4]':
  coordinates_types: [stretch, stretch, angle, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.235546005432286, 1.8443726976347516, -0.3945842693678343, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 2, 3, 5]':
  coordinates_types: [stretch, stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.235546005432286, 1.8443726976347516, -0.3945842693678343, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 2, 3]':
  coordinates_types: [stretch, stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.235546005432286, 1.8443726976347516, -0.3945842693678343]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 2, 4, 5]':
  coordinates_types: [stretch, stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.235546005432286, 1.8443726976347516, -0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 2, 4]':
  coordinates_types: [stretch, stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.235546005432286, 1.8443726976347516, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 2, 5]':
  coordinates_types: [stretch, stretch, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.235546005432286, 1.8443726976347516, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 2]':
  coordinates_types: [stretch, stretch]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [2.235546005432286, 1.8443726976347516]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 3, 4, 5]':
  coordinates_types: [stretch, angle, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.235546005432286, -0.3945842693678343, -0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 3, 4]':
  coordinates_types: [stretch, angle, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.235546005432286, -0.3945842693678343, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 3, 5]':
  coordinates_types: [stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.235546005432286, -0.3945842693678343, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 3]':
  coordinates_types: [stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [2.235546005432286, -0.3945842693678343]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 4, 5]':
  coordinates_types: [stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.235546005432286, -0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 4]':
  coordinates_types: [stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [2.235546005432286, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0, 5]':
  coordinates_types: [stretch, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [2.235546005432286, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[0]':
  coordinates_types: [stretch]
  coupling_significance: 1.0
  expansion_coeff:
  - !!python/object/apply:numpy.core.multiarray.scalar
    - &id001 !!python/object/apply:numpy.dtype
      args: [f8, false, true]
      state: !!python/tuple [3, <, null, null, null, -1, -1, 0]
    - !!binary |
      vGh93YklOr8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      KwPRRdE9tj8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      tPRXuTXKoD8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      aVXTqbEZkz8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      vy5wc69rjj8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      wRpw3Kngiz8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      Wb1wkPqteT8=
  fit_points: '13'
  functional_sequence:
  - [1]
  - [2]
  - [3]
  - [4]
  - [5]
  - [6]
  - [7]
  minimas: [2.235546005432286]
  number_of_points: '21'
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    NMNQyNl0sz4=
  status: Optimized
  valid_points: '8'
'[1, 2, 3, 4, 5]':
  coordinates_types: [stretch, stretch, angle, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1, 1]
  minimas: [2.6172706826066916, 1.8443726976347516, -0.3945842693678343, -0.25595270845356366,
    0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 2, 3, 4]':
  coordinates_types: [stretch, stretch, angle, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.6172706826066916, 1.8443726976347516, -0.3945842693678343, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 2, 3, 5]':
  coordinates_types: [stretch, stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.6172706826066916, 1.8443726976347516, -0.3945842693678343, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 2, 3]':
  coordinates_types: [stretch, stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.6172706826066916, 1.8443726976347516, -0.3945842693678343]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 2, 4, 5]':
  coordinates_types: [stretch, stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.6172706826066916, 1.8443726976347516, -0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 2, 4]':
  coordinates_types: [stretch, stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.6172706826066916, 1.8443726976347516, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 2, 5]':
  coordinates_types: [stretch, stretch, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.6172706826066916, 1.8443726976347516, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 2]':
  coordinates_types: [stretch, stretch]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [2.6172706826066916, 1.8443726976347516]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 3, 4, 5]':
  coordinates_types: [stretch, angle, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [2.6172706826066916, -0.3945842693678343, -0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 3, 4]':
  coordinates_types: [stretch, angle, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.6172706826066916, -0.3945842693678343, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 3, 5]':
  coordinates_types: [stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.6172706826066916, -0.3945842693678343, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 3]':
  coordinates_types: [stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [2.6172706826066916, -0.3945842693678343]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 4, 5]':
  coordinates_types: [stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [2.6172706826066916, -0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 4]':
  coordinates_types: [stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [2.6172706826066916, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1, 5]':
  coordinates_types: [stretch, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [2.6172706826066916, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[1]':
  coordinates_types: [stretch]
  coupling_significance: 1.0
  expansion_coeff:
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      LrNHHLEfe78=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      Wk+0sF4psj8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      KfYJRx2Hjz8=
  fit_points: 0
  functional_sequence:
  - [1]
  - [2]
  - [3]
  minimas: [2.6172706826066916]
  number_of_points: 0
  rmse_newpoints: !!python/object/apply:numpy.core.multiarray.scalar
  - *id001
  - !!binary |
    U3FJHw+zWT8=
  status: NeverSeen
  valid_points: 0
'[2, 3, 4, 5]':
  coordinates_types: [stretch, angle, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1, 1]
  minimas: [1.8443726976347516, -0.3945842693678343, -0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[2, 3, 4]':
  coordinates_types: [stretch, angle, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [1.8443726976347516, -0.3945842693678343, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[2, 3, 5]':
  coordinates_types: [stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [1.8443726976347516, -0.3945842693678343, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[2, 3]':
  coordinates_types: [stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [1.8443726976347516, -0.3945842693678343]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[2, 4, 5]':
  coordinates_types: [stretch, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [1.8443726976347516, -0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[2, 4]':
  coordinates_types: [stretch, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [1.8443726976347516, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[2, 5]':
  coordinates_types: [stretch, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [1.8443726976347516, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[2]':
  coordinates_types: [stretch]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1]
  minimas: [1.8443726976347516]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[3, 4, 5]':
  coordinates_types: [angle, angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1, 1]
  minimas: [-0.3945842693678343, -0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[3, 4]':
  coordinates_types: [angle, angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [-0.3945842693678343, -0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[3, 5]':
  coordinates_types: [angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [-0.3945842693678343, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[3]':
  coordinates_types: [angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1]
  minimas: [-0.3945842693678343]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[4, 5]':
  coordinates_types: [angle, torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1, 1]
  minimas: [-0.25595270845356366, 0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[4]':
  coordinates_types: [angle]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1]
  minimas: [-0.25595270845356366]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
'[5]':
  coordinates_types: [torsion]
  coupling_significance: 1.0
  expansion_coeff: [1.0]
  fit_points: 0
  functional_sequence:
  - [1]
  minimas: [0.0]
  number_of_points: 0
  rmse_newpoints: 1.0
  status: NeverSeen
  valid_points: 0
alpha_basis: !!python/object/apply:numpy.core.multiarray._reconstruct
  args:
  - !!python/name:numpy.ndarray ''
  - !!python/tuple [0]
  - !!binary |
    Yg==
  state: !!python/tuple
  - 1
  - !!python/tuple [6]
  - !!python/object/apply:numpy.dtype
    args: [f8, false, true]
    state: !!python/tuple [3, <, null, null, null, -1, -1, 0]
  - false
  - !!binary |
    9x2aCZM4AUBufV2u5jf1P1h/2azxjvs/KxMfWs3mBkBJwXmhNEX6PwAAAAAAAPA/
boundaries_coordinates:
- &id002 !!python/tuple [1.96, 2.7]
- &id003 !!python/tuple [2.17, 3.74]
- &id004 !!python/tuple [1.5, 2.455]
- &id005 !!python/tuple [-0.76, -0.06]
- &id006 !!python/tuple [-0.89, 0.33]
- &id007 !!python/tuple [0, 3.14]
full_list_of_terms:
- [0]
- [1]
- [2]
- [3]
- [4]
- [5]
- [0, 1]
- [0, 2]
- [0, 3]
- [0, 4]
- [0, 5]
- [1, 2]
- [1, 3]
- [1, 4]
- [1, 5]
- [2, 3]
- [2, 4]
- [2, 5]
- [3, 4]
- [3, 5]
- [4, 5]
- [0, 1, 2]
- [0, 1, 3]
- [0, 1, 4]
- [0, 1, 5]
- [0, 2, 3]
- [0, 2, 4]
- [0, 2, 5]
- [0, 3, 4]
- [0, 3, 5]
- [0, 4, 5]
- [1, 2, 3]
- [1, 2, 4]
- [1, 2, 5]
- [1, 3, 4]
- [1, 3, 5]
- [1, 4, 5]
- [2, 3, 4]
- [2, 3, 5]
- [2, 4, 5]
- [3, 4, 5]
- [0, 1, 2, 3]
- [0, 1, 2, 4]
- [0, 1, 2, 5]
- [0, 1, 3, 4]
- [0, 1, 3, 5]
- [0, 1, 4, 5]
- [0, 2, 3, 4]
- [0, 2, 3, 5]
- [0, 2, 4, 5]
- [0, 3, 4, 5]
- [1, 2, 3, 4]
- [1, 2, 3, 5]
- [1, 2, 4, 5]
- [1, 3, 4, 5]
- [2, 3, 4, 5]
- [0, 1, 2, 3, 4]
- [0, 1, 2, 3, 5]
- [0, 1, 2, 4, 5]
- [0, 1, 3, 4, 5]
- [0, 2, 3, 4, 5]
- [1, 2, 3, 4, 5]
- [0, 1, 2, 3, 4, 5]
local_bounds:
- *id002
- *id003
- *id004
- *id005
- *id006
- *id007
max_order: 3
minimum_coordinates:
- - 2.235546005432286
  - 2.6172706826066916
  - 1.8443726976347516
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      mNscYd5A2b8=
  - !!python/object/apply:numpy.core.multiarray.scalar
    - *id001
    - !!binary |
      Cl0IeIdh0L8=
  - 0.0
- [-205.5319603]
name_of_coordinates: [RO1N, RO2N, RO2H, ANOO, AHON, D]
number_of_dimensions: 6
rmse_target_fit: 1.7086257265620502e-06
rmse_target_newpoints: 2.278167635416067e-06
type_of_coordinates: [stretch, stretch, stretch, angle, angle, torsion]
