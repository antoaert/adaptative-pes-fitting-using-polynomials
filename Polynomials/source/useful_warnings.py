import os
#SOME SORT OF WARNINGS
def warnings(IsThisARestart,DataBasePointsPath,number_of_dimensions,max_order,minimum_coordinates):
    if IsThisARestart:
        if not (os.path.exists(DataBasePointsPath)):
            print('The database file does not exist.')
            exit()
    if max_order > number_of_dimensions:
        print("The largest order of a term cannot be greater than the full dimension of the system")
        exit()
    if len(minimum_coordinates[0])!=number_of_dimensions:
        print("The coordinates of the golbal minimum are not given properly")
        exit()

