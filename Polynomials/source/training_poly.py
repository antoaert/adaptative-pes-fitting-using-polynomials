import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from functools import partial

import numpy as np
from potential import *
from basis_function import *
from scipy.optimize import curve_fit

def training_term_lm(x_train,y_target,expansion_coeff,coordinates_types,functional_sequence,alpha_basis,minimas,**kwargs):
  verbose=kwargs.get('Verbose', False)
  string_network=kwargs.get('name'," ")
  epochs=kwargs.get('epochs',200)
  target_error=kwargs.get('target_error',1.e-5)
  normalization=kwargs.get('scaling',True)
  if normalization:
    minimum_energy=np.min(y_target)
    maximum_energy=np.max(y_target)
    normalization_cste=np.max(abs(y_target))#1.#maximum_energy#-minimum_energy
    for i in range(len(y_target)):
      y_target[i]=y_target[i]/normalization_cste
#  print(len(coordinates_types),"order")
  pfunc=partial(potential_lm,order=len(coordinates_types),coordinates_types=coordinates_types,functional_sequence=functional_sequence,alpha_parameters=alpha_basis,xeq=minimas)
  popt,pcov=curve_fit(pfunc,x_train.T,y_target,p0=expansion_coeff,maxfev=40000000)
  expansion_coeff=list(popt)
  y_out=np.zeros(len(y_target))
  k=0
  for point in x_train:
    y_out[k],dump=potential(point,len(coordinates_types),coordinates_types,functional_sequence,alpha_basis,minimas,list(popt))
    k+=1
  gap=y_target-y_out
  error=(np.square(y_target-y_out))
  error_rmse=np.sqrt(1./len(x_train)*np.sum(error))

  if normalization:
    t=0
    for element in expansion_coeff:
      expansion_coeff[t]=expansion_coeff[t]*float(normalization_cste)
      t+=1
    error_rmse=error_rmse*np.abs(normalization_cste)
    for i in range(len(y_target)):
      y_target[i]=y_target[i]*normalization_cste
      y_out[i]=y_out[i]*normalization_cste
#  print(normalization_cste)
  if len(coordinates_types)>7:
#    x=x_train[:,0]
#    y=x_train[:,1]
#    z=y_target
#    fig = plt.figure()
#    ax = Axes3D(fig)
#    surf = ax.plot_trisurf(x, y, z, cmap=cm.jet, linewidth=0.1)
#    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.plot(x_train,y_target,"ro")
    plt.plot(x_train,y_out)

#    plt.show()
#    fig = plt.figure()
#    ax = Axes3D(fig)
#    surf = ax.plot_trisurf(x, y, gap*normalization_cste, cmap=cm.jet, linewidth=0.1)
#    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()


  return (expansion_coeff,error_rmse,gap)







#We will fit the polynomial expansion using the LM algorithm
def training_term(x_train,y_target,expansion_coeff,coordinates_types,functional_sequence,alpha_basis,minimas,**kwargs):
  verbose=kwargs.get('Verbose', False)
  string_network=kwargs.get('name'," ")
  epochs=kwargs.get('epochs',200)
  target_error=kwargs.get('target_error',1.e-5)
  normalization=kwargs.get('scaling',True)
  mu_inc=10.0 
  mu_dec=0.10
  mu=100.0 #First value of mu
  epoch=0 #Epoch is the integer that follows the iterations of the fit
  old_error=1e+20
  restart=False
  p=0
  m=0
  if normalization:
    minimum_energy=np.min(y_target)
    maximum_energy=np.max(y_target)
    normalization_cste=maximum_energy#-minimum_energy
    for i in range(len(y_target)):
      y_target[i]=y_target[i]/normalization_cste
  while epoch<epochs :
    y_out=np.zeros(len(y_target)) #The output is a 1D array with each value corresponding at the coordinates
    y_1=np.zeros((len(expansion_coeff),len(y_target))) #The derivatives with respect to the error is (#of params, #of points)
    k=0
    for point in x_train:
      y_out[k],y_1[:,k]=potential(point,len(coordinates_types),coordinates_types,functional_sequence,alpha_basis,minimas,expansion_coeff)
      k+=1
    error=(np.square(y_target-y_out))
    gap=y_target-y_out
    #print(gap)
    error_rmse=np.sqrt(1./len(x_train)*np.sum(error))
    if verbose==True:
      print('error_rmse_train',error_rmse)
    #Build the jacobian:
    jacobian=y_1
    hessian=np.dot(jacobian,jacobian.T)
    gradient=np.dot(jacobian,gap.T)
    try:
      HM1=np.linalg.inv(hessian+mu*np.eye(len(hessian[:,0])))
    except np.linalg.LinAlgError:
      print("Not invertible. Go small step in gradient direction")
      weights_update = 1.0/1e10 * gradient
      epoch=epochs #We stop the fitting
    else:
      weights_update=np.dot(-HM1,gradient)
    expansion_coeff_updated=expansion_coeff-weights_update
    #print("update:",weights_update)
    currenterror=np.sum(error)
    if currenterror<old_error:
      p+=1
      m=0
      if p>1:
        mu=mu*mu_dec
        p=0
    elif currenterror==old_error or m==10:
      m=1
      mu=mu*mu_inc
    elif currenterror>old_error and m<10:
      p=0
      m+=1
      mu=mu*mu_inc
      expansion_coeff_updated=expansion_coeff#Go back to the solution with smaller error
    if currenterror<target_error and epoch>49:
      break
#THERE IS NO TEST SET AS FOR NOW
#     if test > old_error_test or test==old_error_test:
#        if g>100:    #The optimization stops if the error test set increases 100 iteratitions after the other
#          break
#        else:
#          g+=1
#      else:
#          g=0
#      old_error_test=test

    expansion_coeff=expansion_coeff_updated #Use the new parameters
    epoch+=1
    if verbose==True:
      print("epoch",epoch)
  if normalization:
    expansion_coeff=expansion_coeff*normalization_cste
    error_rmse=error_rmse*np.abs(normalization_cste)
    for i in range(len(y_target)):
      y_target[i]=y_target[i]*normalization_cste
      y_out[i]=y_out[i]*normalization_cste
  return (expansion_coeff,error_rmse,gap*normalization_cste)

