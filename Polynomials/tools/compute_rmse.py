#!/usr/bin/env python

''' CALL THIS TO COMPUTE TOTAL RMSE'''

import sys
sys.path.append('source/')
from total_rmse import *
import os

if len(sys.argv) < 3:
    raise Exception("Syntax: compute_rmse.py expansion_file mapped_data")

filename_expansion = sys.argv[1]

DataBasePointsPath = sys.argv[2]# str(os.getcwd())+"/"'computed_points/hono_tab_mapped.txt'

total_RMSE(filename_expansion,DataBasePointsPath)
