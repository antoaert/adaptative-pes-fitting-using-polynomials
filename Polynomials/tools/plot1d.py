#!/usr/bin/env python

''' PLOT a 1D TERM FROM THE GIVEN EXPANSION'''

import sys
sys.path.append('source/')

import yaml
import numpy as np
import matplotlib.pyplot as plt
from potential import *


if len(sys.argv) < 3:
    raise Exception("Syntax: plot1d.py expansion_file Coordinate(integer) batchsize(optional, default=100)")

filename_expansion = sys.argv[1]
expansion = yaml.load(open(filename_expansion,'r'), Loader=yaml.Loader)

COORDINATE = int(sys.argv[2])
if len(sys.argv) == 3:
  batchsize = 100  
else:
  batchsize = int(sys.argv[3])


#GENERATE A 1D TERM
str_term='['+str(COORDINATE)+']'
print("Boundaries of the coordinates:", expansion['boundaries_coordinates'])
coord_min=expansion['boundaries_coordinates'][COORDINATE][0]
coord_max=expansion['boundaries_coordinates'][COORDINATE][1]
current_functional_sequence=expansion[str_term]["functional_sequence"]
current_alpha_basis=np.zeros(1)
current_alpha_basis[0]=expansion['alpha_basis'][COORDINATE]
#current_alpha_basis=expansion[str_term]['alpha_basis']
current_minimas=expansion[str_term]['minimas']
current_coordinates_types=expansion[str_term]['coordinates_types']
current_expansion_coeff=expansion[str_term]['expansion_coeff']
x_plot_1D=np.sort(np.random.uniform(low=coord_min, high=coord_max,size=batchsize))
y_out=np.zeros(batchsize)
t=0
for point in x_plot_1D:
  y_out[t]=potential(point,1,current_coordinates_types,current_functional_sequence,current_alpha_basis,current_minimas,current_expansion_coeff)[0]
  t+=1
plt.plot(x_plot_1D,y_out)
plt.show()
