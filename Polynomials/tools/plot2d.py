#!/usr/bin/env python

''' PLOT a 2D TERM FROM THE GIVEN EXPANSION'''

import sys
sys.path.append('source/')

import yaml
import numpy as np
from potential import *

if len(sys.argv) < 3:
    raise Exception("Syntax: plot2d.py expansion_file Coordinate(integer) batchsize(optional, default=100)")

filename_expansion = sys.argv[1]
expansion = yaml.load(open(filename_expansion,'r'), Loader=yaml.Loader)

PAIR = sys.argv[2]
if len(PAIR.split(',')) == 2:
  COORDINATES=[int(PAIR.split(',')[0]),int(PAIR.split(',')[1])]
else:
  raise Exception("Syntax error: the second argument asks for a pair of coordinates (exemple: 0,1)")

if len(sys.argv) == 3:
  batchsize = 500  
else:
  batchsize = int(sys.argv[3])

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
#GENERATE THE 2D TERMS, HOW DO THEY LOOK LIKE ?
str_term='['+str(COORDINATES[0])+', '+str(COORDINATES[1])+']'
print("Boundaries of the coordinates:", expansion['boundaries_coordinates'])
xy_min = [expansion['boundaries_coordinates'][COORDINATES[0]][0],expansion['boundaries_coordinates'][COORDINATES[1]][0]]
xy_max = [expansion['boundaries_coordinates'][COORDINATES[0]][1],expansion['boundaries_coordinates'][COORDINATES[1]][1]]

current_functional_sequence=expansion[str_term]["functional_sequence"]
current_alpha_basis=np.zeros(2)
t=0
for dimension in COORDINATES:
  current_alpha_basis[t]=expansion['alpha_basis'][dimension] #We keep it into the dictionary
  t+=1
#current_alpha_basis=expansion[str_term]['alpha_basis']
current_minimas=expansion[str_term]['minimas']
current_coordinates_types=expansion[str_term]['coordinates_types']
current_expansion_coeff=expansion[str_term]['expansion_coeff']
y_out=np.zeros(batchsize)
points_on_grid=np.random.uniform(low=xy_min, high=xy_max, size=(batchsize,2))
output_expansion=np.zeros(batchsize)
t=0
for point in points_on_grid:
  y_out[t]=potential(point,2,current_coordinates_types,current_functional_sequence,current_alpha_basis,current_minimas,current_expansion_coeff)[0]
  t+=1
x=points_on_grid[:,0]
y=points_on_grid[:,1]
z=y_out

fig = plt.figure()
ax = Axes3D(fig)
surf = ax.plot_trisurf(x, y, z, cmap=cm.jet, linewidth=0.1)
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()
