#!/usr/bin/env python

''' PRINT A MCTDH-READABLE OPERATOR FILE'''

import sys
sys.path.append('tools/')
from mctdh_operator import MCTDH_operator

if len(sys.argv) < 3:
    raise Exception("Syntax: produce_mctdh_operator.py expansion_file operator_name")

filename_expansion = sys.argv[1]
Name_of_the_expansion = 'CORR'  # str(sys.argv[2])# Example: 'CORR'
Operator_file_name = str(sys.argv[2])+'.op'

MCTDH_operator(filename_expansion, Operator_file_name, Name_of_the_expansion)
