#!/usr/bin/env python
import sys
import os
sys.path.append('source/')

import numpy as np
from constants import *
import subprocess
import uuid
from jinja2 import Template
import yaml

from constants import *
from basis_function import *
from potential import *
from itertools import combinations,permutations

def my_function_to_learn(xy,current_term,minimum_coordinates,temperature): #MOLPRO ND HFCO
  if len(current_term)==1:
    xy=[xy]
  data_base_folder=str(os.getcwd())+"/"+"computed_points/"
  if not os.path.exists(data_base_folder):
    os.mkdir(data_base_folder)
  filename_expansion="HONO_3D_CORR.txt"
  expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)
  threshold=expansion['Threshold(cm-1)']/wavenumber_per_ua

  comparison=0
  random_number=0
  current_order=len(current_term)
  if current_order!=1: #THERE ARE NO PARENTS FOR THE 1D cuts
    #We loop over the parent_terms
    total_output=0
    point=xy
    for order in range(1,current_order): #The loop goes up to current_order-1
      for combi in combinations(current_term,order):
        if list(combi)!=[] and len(list(combi))==1:
          parent_term=list(combi)
          if expansion[str(parent_term)]['status']!="Optimized": #Do not consider terms that were skipped
            continue 
          else:
            parent_order=len(parent_term)
            parent_functional_sequence=expansion[str(parent_term)]['functional_sequence'] #what is the sequence of functionals for that term
            t=0
            parent_alpha_basis=np.zeros(parent_order)
            for dimension in parent_term:
              parent_alpha_basis[t]=expansion['alpha_basis'][dimension] 
              t+=1
            parent_minimas=expansion[str(parent_term)]['minimas']
            parent_coordinates_types=expansion[str(parent_term)]['coordinates_types']
            parent_expansion_coeff=expansion[str(parent_term)]['expansion_coeff']
            parent_number_of_points=expansion[str(parent_term)]['number_of_points']
            t=0
            parent_point=np.zeros(parent_order)
            for i in range(parent_order):
              index=np.where(np.array(current_term)==parent_term[i])
              index=int(index[0][0])
              parent_point[i]=point[index]
              #FOR THAT PARENT POINT (ASSOCIATED TO PARENT TERM), WE COMPUTE THE POTENTIAL OUTPUT
            parent_y_output,dump=potential(parent_point,parent_order,parent_coordinates_types,parent_functional_sequence,parent_alpha_basis,parent_minimas,parent_expansion_coeff)
            #We simply dump the derivative here, there is no overhead for its calculation anyways.
            total_output+=parent_y_output
    np.random.seed(int.from_bytes(os.urandom(4), byteorder='little')) #Numpy doesnt play well with multiprocessing
    random_number=np.random.rand(1)[0]
    comparison=(threshold-total_output)/threshold
    if len(current_term)==2:
      comparison=comparison*2 

  #equi_geometry=[1.308,1.83,1.320,30.5,104.5,180.0] #Define first the equilibrium geometry
  geometry=np.zeros(len(minimum_coordinates[0]))
  for i in range(len(minimum_coordinates[0])): #From the number of arguments (len of the list given by adaptative), assign 
    if i in current_term: #->geometry coordinates to the term element (current_term). 
#      print(np.where(np.array(current_term)==i)[0][0])
      geometry[i]=xy[np.where(np.array(current_term)==i)[0][0]]
    else:
      geometry[i]=minimum_coordinates[0][i]

  #There is a MAPPING of the coordinates to do! 
  #Reminder: [RCO,RCH,RCF,AHOC,AFOC,DFHOC]
  #The valence coordinates: [RCO{BOHR},RCH{BOHR},RCF{BOHR},cos(AHOC{RAD}),cos(AHOC{RAD}},DFHOC{RAD}]
  #The procedure will provide the lengths in Bohr, molpro will use them in angstrom
  geometry_mapped=np.zeros(len(geometry))
  geometry_mapped[0]=geometry[0]/angstrom_to_bohr
  geometry_mapped[1]=geometry[1]/angstrom_to_bohr
  geometry_mapped[2]=geometry[2]/angstrom_to_bohr
  geometry_mapped[3]=np.arccos(geometry[3])/radian_per_degree
  geometry_mapped[4]=np.arccos(geometry[4])/radian_per_degree
  geometry_mapped[5]=geometry[5]/radian_per_degree

  if comparison>random_number or len(current_term)==1 :
    #Use this list that define the geometry in the coordinates (and units) of the z-matrix in a template of the molpro calculation. 
    template_molpro_hono='''***, HONO aug-cc-pVTZ-F12 uks
    memory,250,m   
    print orbitals
    geometry={angstrom
    N 
    O 1 RO1N  
    O 1 RO2N 2 ANOO 
    H 3 RO2H 1 AHON 2 D}
   
    !basis=cc-pVTZ
    basis=cc-pVTZ-f12
    RO1N={{r_o1n}}
    RO2N={{r_o2n}}
    RO2H={{r_o2h}}
    ANOO={{a_noo}}
    AHON={{a_hon}}
    D={{d_hono}}
    hf;
    escf=energy
    ccsd(t)-f12;
    eccsdt=energy(1)
    !eccsdtb=energy(2)
    '''
    tm=Template(template_molpro_hono)

    input_file=tm.render(
    r_o1n=geometry_mapped[0],
    r_o2n=geometry_mapped[1],
    r_o2h=geometry_mapped[2],
    a_noo=geometry_mapped[3],
    a_hon=geometry_mapped[4],
    d_hono=geometry_mapped[5])

    #print(input_file)
    unique_filename = str(uuid.uuid4())
    directory_path=str(os.getcwd())+"/"+"hono"+unique_filename+"/"
    if not os.path.exists(directory_path):
      os.mkdir(directory_path)

    input_file_path=directory_path+"hono"+".inp"
    with open(input_file_path, 'w') as f:
       print(input_file, file=f)
    f.close()
    #Once the input file is created, run the program
    exit_status=os.system('''cd '''+str(directory_path)+'''&& /home/nvaeck/Softwares/Molpro2022.2/bin/molpro.exe -v -t 3 '''+'''hono.inp'''+'''>>/dev/null''')
    if exit_status!=0:
      print("The molpro command failed")
      energy=10000000.0
      energy1=10000000.0  
    else:
      #Once this is over, get what you are interested in from the output
      proc = subprocess.Popen('grep -r "SETTING ECCSDT" '+str(directory_path)+'''hono.out | head -1 | cut -d "=" -f 2 | tr -s ' '| cut -d ' ' -f 2 | head -n 1''', stdout=subprocess.PIPE, shell=True)
      #proc = subprocess.Popen("awk '/SETTING ESCF/{print $4;exit}' "+str(directory_path)+'''hono.out''', stdout=subprocess.PIPE, shell=True)
      energy=proc.communicate()[0]
      energy1=energy
      #Keep it somewhere
      path_file_db=data_base_folder+"hono_tab.txt"
      newline=str(current_term).replace(' ','')+' '
      for i in range(len(geometry_mapped)):
        newline+="{:.8f}".format(geometry_mapped[i])+' '
      newline+="{:.8f}".format(float(energy))+' '
      print(newline,file=open(path_file_db,"a"))

      path_file_db_mapped=data_base_folder+"hono_tab_mapped.txt"
      newline=str(current_term).replace(' ','')+' '
      for i in range(len(geometry)):
        newline+="{:.8f}".format(geometry[i])+' '
      newline+="{:.8f}".format(float(energy)-minimum_coordinates[1][0])+' '
      print(newline,file=open(path_file_db_mapped,"a"))
  
    os.system("rm -rf "+str(directory_path)) 
  else:
    #print("point filtered out")
    energy=total_output+minimum_coordinates[1][0]
    energy1=11000./219474.+minimum_coordinates[1][0]
  return {'energ':np.exp(-(float(energy)-minimum_coordinates[1][0])/(temperature/len(current_term)*3.167e-6)),'scf':np.exp(-(float(energy1)-minimum_coordinates[1][0])/(temperature/len(current_term)*3.167e-6))} #ATTENTION RETURN THE OPPOSITE OF THE VALUE IF YOU WANT THE SAMPLING TO BE BETTER AROUND THE MINIMUM RATHER THAN THE MAXIMUM OF THE ENERGY
                                          #WE PROBABLY DON'T WANT THAT FOR THE CORRELATION
