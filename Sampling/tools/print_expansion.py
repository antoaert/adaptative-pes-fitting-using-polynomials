#!/usr/bin/env python
''' PRINT THE EXPANSION TO SCREEN'''
import sys
import yaml

if len(sys.argv) < 2:
    raise Exception("Syntax: print_expansion.py expansion_file")

filename_expansion = sys.argv[1]


# filename_expansion="HONO_3D_CORR.txt"
expansion = yaml.load(open(filename_expansion, 'r'), Loader=yaml.Loader)

print(expansion)
