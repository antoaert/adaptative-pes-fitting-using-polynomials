#this should work just by giving the expansion and appropriate other defined functions.
from itertools import combinations,permutations
from functools import partial
import yaml
from constants import *
from basis_function import *
from potential import *
from training_poly import *
from useful_functions import *
import adaptive
from my_function_to_learn import *
from concurrent.futures import ProcessPoolExecutor
import math
import numpy as np
from scipy.optimize import curve_fit
from operator import itemgetter
'''This is the core of the program, it builds the expansion of the PES functional by computing abinitio points and fitting the
corresponding terms of the HDMR expansion that parametrize the form of the PES.'''

def sample_pes(filename_expansion,DoISaveTheExpansion,IsThisARestart,DataBasePointsPath,Verbose_Comment,local_energy_threshold,n_workers,temperature):
  expansion = yaml.load(open(filename_expansion, 'r'))
  os.system("touch ./stop")

  '''The program (should) handle a restart. Attention! The "mapped" data has to be provided (In the units and coordinates of the fit!,
  Not the coordinates used by the abinitio program!)'''

  if IsThisARestart:
    database_restart=np.genfromtxt(DataBasePointsPath,dtype=None,encoding=None)

  t=0
  #WE LOOP OVER THE TERMS OF THE EXPANSION TO CONSTRUCT IT
  for current_term in expansion['full_list_of_terms']:
    warning=0 #There is a warning that we don't want to see too many times.
    done=False #Optimize the alpha parameter only once for each coordinate.
    current_order=len(current_term) #NUMBER OF COORDINATES THAT CONCERN THIS TERM

    #TEST IF WE NEED TO SKIP THE TERM BASED ON USER INPUT
    if current_order>expansion['max_order']:
      expansion[str(current_term)]["status"]="Skip"
      print("The order of this term exceeds the maximum order of the expansion set by the user.")
    if expansion[str(current_term)]["status"]=="Skip":
      print("term:"+str(current_term)+" is skipped.")
      need_more_points=False
    else:
      need_more_points=True
      print("Working on term:"+str(current_term))

    current_minimas=expansion[str(current_term)]['minimas']
    current_coordinates_types=expansion[str(current_term)]['coordinates_types']
    boundaries_coordinates=expansion['boundaries_coordinates']
    local_boundaries=expansion['local_bounds']

    '''The procedure will compute ab-initio points until it meets a convergence criterion. To stop computing points, it relies on a 
    number of criteria.
    If these criteria are met, the the need_more_points variable is set to False.'''

    iter=0


    '''The sampling of the data points is handled by the 'adaptive' library (see https://adaptive.readthedocs.io/en/latest/ for details).
    Using the library would require to define a function to be learned (here the energy with respect to the coordinates) for each term of 
    the HDMR expansion. Instead we use the partial function of python. The adaptive learner is defined here. There is a 1D version and 
    ND version of the learner, details are in the documentation of the library.
    
    The learner is given coordinates boundaries to start with, this was defined by the user.'''

    #WE NEED TO INITIATE THE LEARNER OF THE TERM OUTSIDE THE LOOP OR IT WILL HAVE TO RECOMPUTE EVERYTHING AT EACH ITER
    #USE OF PARTIAL : https://www.geeksforgeeks.org/partial-functions-python/
    if current_order==1:
#      learner = adaptive.Learner1D(partial(my_function_to_learn,current_term=current_term,minimum_coordinates=expansion['minimum_coordinates']),(boundaries_coordinates[current_term[0]][0], boundaries_coordinates[current_term[0]][1]))
      _learner = adaptive.Learner1D(partial(my_function_to_learn,current_term=current_term,minimum_coordinates=expansion['minimum_coordinates'],temperature=temperature),(local_boundaries[current_term[0]][0], local_boundaries[current_term[0]][1]))
      learner = adaptive.DataSaver(_learner, arg_picker=itemgetter('energ'))

    elif current_order>=2:
    #BASED ON THE TERM, WE NEED THE APPROPRIATE BOUNDS
      current_bounds=[]
      for i in current_term:
        current_bounds.append(boundaries_coordinates[i])
      _learner = adaptive.LearnerND(partial(my_function_to_learn,current_term=current_term,minimum_coordinates=expansion['minimum_coordinates'],temperature=temperature), bounds=current_bounds)
      learner= adaptive.DataSaver(_learner, arg_picker=itemgetter('energ'))


    while need_more_points:   #-> Adapt a loss function to exlcude high energy regions? We used a workaround.
      iter+=1
      #if iter==1:#GENERATE THE FIRST POINTS FOR THE TERM (THE DATA IS TERM SPECIFIC!)  
      old_y_target=[]
      if iter>1:
        old_x_train=current_x_train
        old_y_target=current_y_target #THIS WILL HAVE THE EFFECT OF THE PARENT IF APPLICABLE!


      '''We define the goal of the sampling procedure, this will be adapted as we go forward in the learning process.
      The goal is defined either by a minimum number of points to be sample:
      $npoints_goal = lambda l: l.npoints >= 2*iter
      
      Or one can also define the maximal width of the feature to be leaned. This has more sense when your data has peaks. This goal is then
      the maximum width of the peaks that you want to find. One need to use the two lines :
      $a=(boundaries_coordinates[current_term[0]][1]-boundaries_coordinates[current_term[0]][0])/(float(iter)/10.)
      $nloss= lambda l: l.loss()<a'''


      if current_order==1:
        npoints_goal = lambda l: l.npoints >= n_workers*iter
        #a=(boundaries_coordinates[current_term[0]][1]-boundaries_coordinates[current_term[0]][0])/(float(iter)/10.)
        #nloss= lambda l: l.loss()<a

        #GIVE OUR INSIGHTS TO THE LEARNER, IT WILL HELP TO KNOW WHERE THE GLOBAL MINIMUM IS --> this doesn't work for the correlation part ...
#       learner.tell(minimum_coordinates[0][current_term[0]],0.0) #Our data will be set to zero at the minimym by the function that is to be learned, so the minimum is zero


        '''In case of a restart, we need to feed the learner with the appropriate points.'''
      
        #TELL THE LEARNER ABOUT THE POINTS IN THE DATABASE
        if IsThisARestart and iter==1:
          iter+=1  
          current_known_points=[]
          for item in database_restart:
            if item[0]==(str(current_term)).replace(' ', ''):
              current_known_points.append(item)            
          old_y_target=np.copy(current_known_points)
#          print(len(old_y_target),len(current_known_points))
          print("We found ",len(current_known_points)," points in the database.")
          if current_order==1 and (current_coordinates_types=='stretch' or current_coordinates_types=='angle'): 
            expansion[str(current_term)]['expansion_coeff']=[1.0]
          else:
            expansion[str(current_term)]['expansion_coeff']=[1.0]
          for x in range(len(current_known_points)):
            point=np.zeros(current_order)
            for u in range(current_order):
              point[u]=current_known_points[x][current_term[u]+1]
            '''Here is the first instance of the 'WasTheSignChanged' boolean, this is actually a workaround for the sampling library. The library
            will assign a greater importance to points that have a large (positive) value. Therefore, as-is the high energy regions will be better
            sampled. This is absolutely not what we want! The simplest workaround was to sample the opposite value of the energy by adaptive. This
            way, the minimum value of the energy is the point that adaptive would give the most importance and will sample preferably the low 
            energy regions. This boolean is used in several places to make sure that we fit the right sign of the energy but that adaptive keeps
            its own vision of the energy.''' 

            if expansion['WasTheSignChanged']:
              _learner.tell(point[0],current_known_points[x][-1]+expansion['Threshold(cm-1)']/wavenumber_per_ua)
            else:
              _learner.tell(point[0],current_known_points[x][-1])
          learner= adaptive.DataSaver(_learner, arg_picker=itemgetter('energ'))
        #SAMPLING 1D happens here:
        if expansion['VerboseComment']:
          print("Sampling of new points")

        '''We can define the executor of the sampling to use multiple cores, this is experimental at the moment !!!'''

        executor = ProcessPoolExecutor(max_workers=n_workers)
        adaptive.BlockingRunner(learner,goal=npoints_goal,executor=executor)
#        adaptive.BlockingRunner(learner,goal=nloss,executor=executor)
        #RESULTS:
        current_x_train=np.copy(list(learner.data.keys()))
        current_y_target=np.copy(list(learner.data.values()))
        current_dict=np.array(list(learner.extra_data.items()))
        scf_energy=np.zeros(len(current_dict))
        for i in range(len(current_dict)):
          scf_energy[i]=current_dict[i][1]['scf']
          if expansion['WasTheSignChanged']:
            scf_energy[i]=-np.log(scf_energy[i])*temperature*3.167e-6

        if expansion['WasTheSignChanged']:     #Since we use the workaround to make the procedure sample more around the minimum, we need to change the sign of the data used in all procedures but the sampling
          p=0
          for value in current_y_target:
            current_y_target[p]=-np.log(value)*temperature*3.167e-6
            p+=1

        '''The points that are above the threshold do not need to be given to the fitting procedure.
        For the 1D term, the user has to define local energy bounds. It might be useful to converge the 1D terms up to a larger energy
        as compared to the ND terms (N>1) since it will ease the fitting of the ND terms and is usually not costly.'''

        #WE HAVE TO REMOVE POINTS THAT ARE ABOVE THE THRESHOLD
        if expansion['EnergyThreshold']:
          energy_threshold=expansion['Threshold(cm-1)']/wavenumber_per_ua
          number_of_deletion=len(np.where(scf_energy>local_energy_threshold)[0])
           
          if number_of_deletion>0:
            current_x_train=np.delete(current_x_train, np.where(scf_energy>local_energy_threshold)[0], axis=0)
            current_y_target=np.delete(current_y_target, np.where(scf_energy>local_energy_threshold)[0], axis=0)
          if expansion['VerboseComment']:
            print("Deleted ", number_of_deletion," points above the Threshold")
            print("Current ytarget has ",len(current_y_target)," points")
            print("Deleted len",len(current_x_train),len(list(learner.data.values())))  

      elif current_order>=2:
        ''' All above steps are taken for the higher order terms as well, except the optimization of the non-linear coefficients.'''
        npoints_goal = lambda l: l.npoints > iter*n_workers #This sets a number of points to compute
        a=(boundaries_coordinates[current_term[0]][1]-boundaries_coordinates[current_term[0]][0])/(0.25*float(iter))
        nloss= lambda l: l.loss()<a
        #npoints_goal = lambda l: l.npoints >= 2*iter
        #TELL THE LEARNER ABOUT THE POINTS IN THE DATABASE
        if IsThisARestart and iter==1:
          iter+=1
          current_known_points=[]
          for item in database_restart:
            if item[0]==(str(current_term)).replace(' ', ''):
              current_known_points.append(item)
          old_y_target=np.copy(current_known_points)
          print("We found ",len(current_known_points)," points in the database.")
          if len(current_known_points)!=0:
            for x in range(len(current_known_points)):
              point=np.zeros(current_order)
              for u in range(current_order):
                point[u]=current_known_points[x][current_term[u]+1]
             
              if expansion['WasTheSignChanged']:
                _learner.tell(point,current_known_points[x][-1]+expansion['Threshold(cm-1)']/wavenumber_per_ua)
              else:
                _learner.tell(point,current_known_points[x][-1])
          learner= adaptive.DataSaver(_learner, arg_picker=itemgetter('energ'))
        executor = ProcessPoolExecutor(max_workers=n_workers)
        adaptive.BlockingRunner(learner,goal=npoints_goal,executor=executor)
        current_x_train=np.copy(list(learner.data.keys()))
        current_y_target=np.copy(list(learner.data.values()))
        current_dict=np.array(list(learner.extra_data.items()),dtype=object)
        scf_energy=np.zeros(len(current_dict))
        for i in range(len(current_dict)):
          scf_energy[i]=current_dict[i][1]['scf']
          if expansion['WasTheSignChanged']:     
            scf_energy[i]=-np.log(scf_energy[i])*temperature/current_order*3.167e-6
            #print(scf_energy[i])
      
        if expansion['WasTheSignChanged']:     #Since we use the workaround to make the procedure sample more around the minimum, we need to change the sign of the data used in all procedures but the sampling
          p=0
          for value in current_y_target:
            current_y_target[p]=-np.log(value)*temperature/current_order*3.167e-6
            p+=1
        #WE HAVE TO REMOVE POINTS THAT ARE ABOVE THE THRESHOLD
        
        if expansion['EnergyThreshold']:
          energy_threshold=expansion['Threshold(cm-1)']/wavenumber_per_ua
          number_of_deletion=len(np.where(scf_energy>energy_threshold))
          if number_of_deletion>0:
            if expansion['VerboseComment']:
              print("Deleted point(s) above the threshold")
            current_x_train=np.delete(current_x_train, np.where(scf_energy>energy_threshold)[0], axis=0)
            current_y_target=np.delete(current_y_target, np.where(scf_energy>energy_threshold)[0], axis=0)
          if expansion['VerboseComment']:
            print("Deleted ", number_of_deletion," points above the Threshold")
            print("Current ytarget has ",len(current_y_target)," points")

      number_of_new_points=len(current_y_target)-len(old_y_target)

    #THE DATA THAT NEEDS TO BE FITTED IS THE DIFFERENCE WITH THE PARENT TERMS
    #HOW TO GET PARENT TERMS ?
      if (len(current_y_target!=0)) and (expansion[str(current_term)]['status']!='Skip') and (len(current_y_target)>2): 
        if iter !=1:
          if number_of_new_points>0: 
            if (len(current_y_target)>20*current_order):
              need_more_points=False
          else:
            need_more_points=True
            iter+=1


